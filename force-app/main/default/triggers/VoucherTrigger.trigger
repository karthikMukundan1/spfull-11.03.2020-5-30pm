/*******************************************************************************
@author:		Donnie Banez
@date:         	Feb 2018
@description:  	Trigger for Voucher 
@Test Methods:	VoucherTriggerHandlerTest 
*******************************************************************************/
trigger VoucherTrigger on Voucher__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Contact Trigger Handler 
    VoucherTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}