trigger accountSplitChildrenAge on Account (after insert, after update) {

String strChildrenAge = '';
    Id[] pacctidsC = new Id[]{};
	List<String> lChildrenAge = new List<String>();
	
	Model_Of_Interest__c[] tempChildrenAge = new Model_Of_Interest__c[]{};
	if (Trigger.isUpdate){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){
		    	if (Trigger.oldMap.get(a.Id).Children_s_Age3__pc != a.Children_s_Age3__pc){  
		    	pacctidsC.add(a.Id);      					
					if(a.Children_s_Age3__pc != null){
									        
				        strChildrenAge = a.Children_s_Age3__pc;	
				        lChildrenAge= strChildrenAge.split(';');
				        for(Integer i = 0; i< lChildrenAge.size(); i++){
				        	Model_Of_Interest__c moca = new Model_Of_Interest__c();
				        	moca.Account__c = a.Id;
				        	moca.Age_Group__c = lChildrenAge[i];
				        	moca.RecordTypeId = '012900000000XAJAA2';
				        	moca.Category__c = 'Children Age';		        				        	
	        				tempChildrenAge.add(moca);
	        			}	
					}
		    	}			        		  
		    }              		 	     	                    
		}
	}else if (Trigger.isInsert){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){
		    	  
		    	pacctidsC.add(a.Id);      					
					if(a.Children_s_Age3__pc != null){
									        
				        strChildrenAge = a.Children_s_Age3__pc;	
				        lChildrenAge= strChildrenAge.split(';');
				        for(Integer i = 0; i< lChildrenAge.size(); i++){
				        	Model_Of_Interest__c moca = new Model_Of_Interest__c();
				        	moca.Account__c = a.Id;
				        	moca.Age_Group__c = lChildrenAge[i];
				        	moca.RecordTypeId = '012900000000XAJAA2';
				        	moca.Category__c = 'Children Age';		        				        	
	        				tempChildrenAge.add(moca);
	        			}	
					}
		    				        		  
		    }              		 	     	                    
		}
	}
    Model_Of_Interest__c[] tempChi = new Model_Of_Interest__c[]{};
    if(pacctidsC.size() > 0){
       	tempChi = [select Id from Model_Of_Interest__c where Account__c IN :pacctidsC AND RecordTypeId = '012900000000XAJAA2' and Category__c = 'Children Age']; 	
    	if(tempChi.size() > 0){
    		delete tempChi;
    	}
    }
    
	if(tempChildrenAge.size() > 0){
		insert tempChildrenAge;
	}

}