/*******************************************************************************
@author:		Donnie Banez
@date:         	March 2019
@description:  	Trigger for Offer 
@Test Methods:	VehicleOwnershipTriggerHandlerTest 
*******************************************************************************/
trigger VehicleOwnershipTrigger on Vehicle_Ownership__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Vehicle Ownership Trigger Handler 
    VehicleOwnershipTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}