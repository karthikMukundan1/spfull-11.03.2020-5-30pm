trigger accountSplitReasonForPurchase on Account (after insert, after update) {

String strReasonForPurchase = '';
    Id[] pacctidsR = new Id[]{};
	List<String> lReasonForPurchase = new List<String>();
	
	Model_Of_Interest__c[] tempReasonForPurchase = new Model_Of_Interest__c[]{};
	if (Trigger.isUpdate){
		for (Account a : Trigger.new) {
		   
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){
		    	if (Trigger.oldMap.get(a.Id).Reason_For_Purchase__pc != a.Reason_For_Purchase__pc){  
		    	pacctidsR.add(a.Id);  	    	    					
					if(a.Reason_For_Purchase__pc != null){
									        
				        strReasonForPurchase = a.Reason_For_Purchase__pc;	
				        lReasonForPurchase= strReasonForPurchase.split(';');
				        for(Integer i = 0; i< lReasonForPurchase.size(); i++){
				        	Model_Of_Interest__c moca = new Model_Of_Interest__c();
				        	moca.Account__c = a.Id;
				        	moca.Model__c = lReasonForPurchase[i];
				        	moca.RecordTypeId = '012900000000XAJAA2';
				        	moca.Category__c = 'Reason For Purchase';		        				        	
	        				tempReasonForPurchase.add(moca);
	        			}	
					}
		    	}			        		  
		    }              		 	     	                    
		}
	}else if (Trigger.isInsert){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){
		    	  
		    	pacctidsR.add(a.Id);      					
					if(a.Reason_For_Purchase__pc != null){
									        
				        strReasonForPurchase = a.Reason_For_Purchase__pc;	
				        lReasonForPurchase= strReasonForPurchase.split(';');
				        for(Integer i = 0; i< lReasonForPurchase.size(); i++){
				        	Model_Of_Interest__c moca = new Model_Of_Interest__c();
				        	moca.Account__c = a.Id;
				        	moca.Model__c = lReasonForPurchase[i];
				        	moca.RecordTypeId = '012900000000XAJAA2';
				        	moca.Category__c = 'Reason For Purchase';		        				        	
	        				tempReasonForPurchase.add(moca);
	        			}	
					}
		    				        		  
		    }              		 	     	                    
		}
	}
    Model_Of_Interest__c[] tempRea = new Model_Of_Interest__c[]{};
    if(pacctidsR.size() > 0){
       	tempRea = [select Id from Model_Of_Interest__c where Account__c IN :pacctidsR AND RecordTypeId = '012900000000XAJAA2' and Category__c = 'Reason For Purchase']; 	
    	if(tempRea.size() > 0){
    		delete tempRea;
    	}
    }
    
	if(tempReasonForPurchase.size() > 0){
		insert tempReasonForPurchase;
	}

}