/*******************************************************************************
@author:		Donnie Banez
@date:         	Aug 2019
@description:  	Trigger for Asset__c 
@Test Methods:	AssetTriggerHandlerTest 
*******************************************************************************/
trigger AssetTrigger on Asset__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Offer Trigger Handler 
    AssetTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}