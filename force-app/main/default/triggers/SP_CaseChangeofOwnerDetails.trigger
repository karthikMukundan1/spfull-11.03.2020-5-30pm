trigger SP_CaseChangeofOwnerDetails on Case (before insert)
{
	set<id>				set_ContactIds			= new set<id>();
	set<id>				set_AccountIds			= new set<id>();
	map<id, id>			map_ContacttoAccount	= new map<id, id>();
	map<id, set<id>>	map_OwnertoDealerIds	= new map<id, set<id>>();

	// Get a set of all Contacts related to the new Case
	// Ignore if this is a change of details from a Dealer website
	for (Case sCase : trigger.new)
	{
		if (sCase.Type == 'Update Details' && sCase.Origin != SPCacheTWAMetadata.getCaseOriginDealerWeb())
		{
			set_ContactIds.add(sCase.ContactId);
		}
	}

	// Get the Person Accounts for these Contact Ids
	for (Contact [] arrContact :	[	select	AccountId
										from	Contact
										where	Id in :set_ContactIds
									])
	{
		for (Contact sContact : arrContact)
		{
			set_AccountIds.add(sContact.AccountId);
			map_ContacttoAccount.put(sContact.Id, sContact.AccountId);
		}
	}

	// Now identify all Dealers associated with the Owners via the active VOs
	for (Vehicle_Ownership__c [] arrVO :	[	select	Id,
														Customer__c,
														Branch_of_Dealership__c,
														Vehicle_Type__c,
														Sales_Consultant__c
												from	Vehicle_Ownership__c
												where	Customer__c in :set_AccountIds
												and		Status__c = 'Active'
											])
	{
		for (Vehicle_Ownership__c sVO : arrVO)
		{
			if	(	(	sVO.Vehicle_Type__c == SPCacheVehicleTypeMetadata.getDealerTradeIn() ||
						sVO.Vehicle_Type__c == SPCacheVehicleTypeMetadata.getDemonstration() ||
						sVO.Vehicle_Type__c == SPCacheVehicleTypeMetadata.getNewVehicle() ||
						sVO.Vehicle_Type__c == SPCacheVehicleTypeMetadata.getWriteOff()
					)
					||
					(	sVO.Vehicle_Type__c == SPCacheVehicleTypeMetadata.getPre_Owned() &&
						sVO.Sales_Consultant__c != null
					)
				)
			{
				if (sVO.Branch_of_Dealership__c != null)
				{
					if (!map_OwnertoDealerIds.containsKey(sVO.Customer__c))
					{
						set<id> set_Dealers = new set<id>();
						set_Dealers.add(sVO.Branch_of_Dealership__c);
						map_OwnertoDealerIds.put(sVO.Customer__c, set_Dealers);
					}
					else
					{
						set<id> set_Dealers = map_OwnertoDealerIds.remove(sVO.Customer__c);
						set_Dealers.add(sVO.Branch_of_Dealership__c);
						map_OwnertoDealerIds.put(sVO.Customer__c, set_Dealers);
					}
				}
			}
		}
	}

	// Finally, populate the Case with up to four Dealer Ids
	for (Case sCase : trigger.new)
	{
		if (map_OwnertoDealerIds.containsKey(map_ContacttoAccount.get(sCase.ContactId)))
		{
			set<id> set_Dealers = map_OwnertoDealerIds.get(map_ContacttoAccount.get(sCase.ContactId));
			list<id> li_Dealers = new list<id>();

			for (id idDealerId : set_Dealers)
			{
				li_Dealers.add(idDealerId);
			}

			if (li_Dealers.size() > 3)
				sCase.TempCOODDealer4__c	= li_Dealers[3];

			if (li_Dealers.size() > 2)
				sCase.TempCOODDealer3__c	= li_Dealers[2];

			if (li_Dealers.size() > 1)
				sCase.TempCOODDealer2__c	= li_Dealers[1];

			if (li_Dealers.size() > 0)
				sCase.TempCOODDealer1__c	= li_Dealers[0];
		}
	}
}