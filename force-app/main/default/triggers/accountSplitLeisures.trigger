trigger accountSplitLeisures on Account (after insert, after update) {

	String strLeisure = '';
   
    Id[] pacctidsL = new Id[]{};
	List<String> lLeisure = new List<String>();	
	Model_Of_Interest__c[] tempLeisure = new Model_Of_Interest__c[]{};
	
	if (Trigger.isUpdate){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){  
		    	if (Trigger.oldMap.get(a.Id).Leisure_Time_Activities__pc != a.Leisure_Time_Activities__pc){    		
					pacctidsL.add(a.Id);			
					if(a.Leisure_Time_Activities__pc != null){
									        
				        strLeisure = a.Leisure_Time_Activities__pc;	
				        lLeisure = strLeisure.split(';');
				        for(Integer i = 0; i< lLeisure.size(); i++){
				        	Model_Of_Interest__c mol = new Model_Of_Interest__c();
				        	mol.Account__c = a.Id;
				        	mol.Model__c = lLeisure[i];
				        	mol.RecordTypeId = '012900000000XAJAA2';
				        	mol.Category__c = 'Leisure Time Activity';		        				        	
	        				tempLeisure.add(mol);
	        			}	
					}
		    	}						        		  
		     }              		 	     	                    
		}
	}else if (Trigger.isInsert){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){  
		    	    		
					pacctidsL.add(a.Id);			
					if(a.Leisure_Time_Activities__pc != null){
									        
				        strLeisure = a.Leisure_Time_Activities__pc;	
				        lLeisure = strLeisure.split(';');
				        for(Integer i = 0; i< lLeisure.size(); i++){
				        	Model_Of_Interest__c mol = new Model_Of_Interest__c();
				        	mol.Account__c = a.Id;
				        	mol.Model__c = lLeisure[i];
				        	mol.RecordTypeId = '012900000000XAJAA2';
				        	mol.Category__c = 'Leisure Time Activity';		        				        	
	        				tempLeisure.add(mol);
	        			}	
					}
		    							        		  
		     }              		 	     	                    
		}
	}
	
	Model_Of_Interest__c[] tempLei = new Model_Of_Interest__c[]{};
    if(pacctidsL.size() > 0){
       	tempLei = [select Id from Model_Of_Interest__c where Account__c IN :pacctidsL AND RecordTypeId = '012900000000XAJAA2' and Category__c = 'Leisure Time Activity']; 	
    	if(tempLei.size() > 0){
    		delete tempLei;
    	}
    }

	if(tempLeisure.size() > 0){
		insert tempLeisure;
	}


}