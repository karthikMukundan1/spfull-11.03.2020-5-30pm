trigger NewWarrantyUpdateAccount on Warranty__c (after insert, after update) 
{
    Set<Id> accountIds = new Set<Id>();
    
    for(Warranty__c warr : Trigger.new)
    {
        if(warr.Warranty_Type__c == 'Factory Warranty')
        {
            if(trigger.isInsert || (trigger.isUpdate && trigger.old[0].Expiration_Date__c != warr.Expiration_Date__c))
            {
                accountIds.add(warr.Customer_Account__c);
                /*
                List<Account> newOwner = [SELECT Id, Encore_Expiration_Date__c, Factory_Warranty_Expiration_Date__c
                                                        FROM Account WHERE Id = :warr.Customer_Account__c];
                
                Warranty__c[] curFactWarList = [SELECT Customer_Account__c, Expiration_Date__c  FROM Warranty__c
                                                        WHERE Customer_Account__c = :warr.Customer_Account__c 
                                                        AND Warranty_Type__c = 'Factory Warranty'
                                                        ORDER BY Expiration_Date__c DESC LIMIT 1];
            
                for(Account newOwn :newOwner)
                {                                   
                    if(curFactWarList.size() > 0)   
                    {
                        newOwn.Encore_Expiration_Date__c = curFactWarList[0].Expiration_Date__c;
                        newOwn.Factory_Warranty_Expiration_Date__c = curFactWarList[0].Expiration_Date__c;
                    }
                }
                
                update newOwner;
                */
            }
        }
    }
    if(accountIds.isEmpty()) return;
    System.debug('NewWarrantyUpdateAccount accountIds: ' + accountIds);
    
    List<Account> accounts = [SELECT Id, Encore_Expiration_Date__c, Factory_Warranty_Expiration_Date__c,
                              (select Customer_Account__c, Expiration_Date__c  from Warranties1__r 
                              where Warranty_Type__c = 'Factory Warranty' ORDER BY Expiration_Date__c DESC LIMIT 1)
                              FROM Account WHERE Id in: accountIds];
    for(Account account : accounts) {
        if(account.Warranties1__r.isEmpty() == false) {
            // 21 JAN 2020: D BANEZ: Commented out as part of de-rdr trigger project; Field will be updated directly by Encore
            //account.Encore_Expiration_Date__c = account.Warranties1__r[0].Expiration_Date__c;
            account.Factory_Warranty_Expiration_Date__c = account.Warranties1__r[0].Expiration_Date__c;
        }
    }
    
    if (!accounts.isEmpty())
    	SP_Utilities.AccountUpdatedByWarrantyTrigger = true;
    	
    update accounts;
    
}