trigger Roadside_Assistant_CodeSearch on Case (before update) {
	
	set<String> breakDownCodeList = new set<String>{};
	for(Case ca: trigger.new ){
		if(ca.RecordTypeId != '012900000000YXq'){continue;}
		if (ca.Breakdown_Code__c != null && (breakDownCodeList.contains(ca.Breakdown_Code__c)== false)){breakDownCodeList.add(ca.Breakdown_Code__c);}
	}
	
	if (breakDownCodeList.size()>0){
	
		Configuration__c[] config = new Configuration__c[]{};
		map<String,Configuration__c> configList = new map<String,Configuration__c>{};
		config = [select Name, Description__c,Owner_Or_Vehicle__c,Breakdown_Catalogue__c from Configuration__c where Name in :breakDownCodeList];
		if (config.size()> 0)
		{
			for (Configuration__c cf: config){
				configList.put(cf.Name, cf);
			}
		}
		
		for (Case ca: trigger.new){
			//system.assertEquals(configList.get(ca.Breakdown_Code__c).Description__c, ca.Description);
			if (configList.containsKey(ca.Breakdown_Code__c)){
				ca.Description = configList.get(ca.Breakdown_Code__c).Description__c;
				ca.Subject = configList.get(ca.Breakdown_Code__c).Owner_Or_Vehicle__c;}
			if (configList.containsKey(ca.Breakdown_Code__c)== false){
				//system.assertEquals(ca.Breakdown_Code__c, null, 'Can not find matching breakdown code!');
				ca.Description = 'Can not find matching breakdown code!';
				ca.Subject = 'Can not find matching breakdown code!';
			}
		}
	}

}