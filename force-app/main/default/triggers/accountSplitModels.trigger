// Production
// 012900000000XAJAA2 Model of Interest Account RT
// 012900000000THwAAM Customer
// 012900000000UJyAAM Prospect
// Model of Interests
// Leisure of Activity
// Sport Interests


trigger accountSplitModels on Account (after insert, after update) {
	
	String strModel = '';
    Id[] pacctidsM = new Id[]{};
	List<String> lModel = new List<String>();
	Model_Of_Interest__c[] tempModel = new Model_Of_Interest__c[]{};
		
	if (Trigger.isUpdate){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){
		      
				if (Trigger.oldMap.get(a.Id).Model_of_Interest__c != a.Model_of_Interest__c){  		
						pacctidsM.add(a.Id);
						if(a.Model_of_Interest__c != null){
									        
				        	strModel = a.Model_of_Interest__c;	
				        	lModel = strModel.split(';');
				        	for(Integer i = 0; i< lModel.size(); i++){
					        	Model_Of_Interest__c mo = new Model_Of_Interest__c();
					        	mo.Account__c = a.Id;
					        	mo.Model__c = lModel[i];
					        	mo.RecordTypeId = '012900000000XAJAA2';
					        	mo.Category__c = 'Model Of Interest';		        				        	
		        				tempModel.add(mo);
	        				}	
						}
		    	}		        		  
		   	}              		 	     	                    
		}
	}else if(Trigger.isInsert){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){
		      		
						pacctidsM.add(a.Id);
						if(a.Model_of_Interest__c != null){
									        
				        	strModel = a.Model_of_Interest__c;	
				        	lModel = strModel.split(';');
				        	for(Integer i = 0; i< lModel.size(); i++){
					        	Model_Of_Interest__c mo = new Model_Of_Interest__c();
					        	mo.Account__c = a.Id;
					        	mo.Model__c = lModel[i];
					        	mo.RecordTypeId = '012900000000XAJAA2';
					        	mo.Category__c = 'Model Of Interest';		        				        	
		        				tempModel.add(mo);
	        				}	
						}
		    			        		  
		   	}              		 	     	                    
		}
	}
	
	Model_Of_Interest__c[] tempMo = new Model_Of_Interest__c[]{};
    if(pacctidsM.size() > 0){
       	tempMo = [select Id from Model_Of_Interest__c where Account__c IN :pacctidsM AND RecordTypeId = '012900000000XAJAA2' and Category__c = 'Model Of Interest']; 	
    	if(tempMo.size() > 0){
    		delete tempMo;
    	}
    }
    
	if(tempModel.size() > 0){
		insert tempModel;
	}

}