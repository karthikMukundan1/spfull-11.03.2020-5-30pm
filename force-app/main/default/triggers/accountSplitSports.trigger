trigger accountSplitSports on Account (after insert, after update) {


	String  strSport = '';
    Id[] pacctidsS = new Id[]{};
	List<String> lSport = new List<String>();	
	Model_Of_Interest__c[] tempSport = new Model_Of_Interest__c[]{};

	if (Trigger.isUpdate){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){ 
		    	if (Trigger.oldMap.get(a.Id).Sports_Interest__pc != a.Sports_Interest__pc){       		
					pacctidsS.add(a.Id);		
					if(a.Sports_Interest__pc != null){
								        
				        strSport = a.Sports_Interest__pc;	
				        lSport= strSport.split(';');
				        for(Integer i = 0; i< lSport.size(); i++){
				        	Model_Of_Interest__c mos = new Model_Of_Interest__c();
				        	mos.Account__c = a.Id;
				        	mos.Model__c = lSport[i];
				        	mos.RecordTypeId = '012900000000XAJAA2';
				        	mos.Category__c = 'Sport Interest';		        				        	
	        				tempSport.add(mos);
	        			}	
					}
		    	}
		    }
		}
	} else if (Trigger.isInsert){
		for (Account a : Trigger.new) {
		    if (a.RecordTypeId == '012900000000THwAAM' || a.RecordTypeId == '012900000000UJyAAM'){ 
		    	       		
					pacctidsS.add(a.Id);		
					if(a.Sports_Interest__pc != null){
								        
				        strSport = a.Sports_Interest__pc;	
				        lSport= strSport.split(';');
				        for(Integer i = 0; i< lSport.size(); i++){
				        	Model_Of_Interest__c mos = new Model_Of_Interest__c();
				        	mos.Account__c = a.Id;
				        	mos.Model__c = lSport[i];
				        	mos.RecordTypeId = '012900000000XAJAA2';
				        	mos.Category__c = 'Sport Interest';		        				        	
	        				tempSport.add(mos);
	        			}	
					}
		    	
		    }
		}
	}
	Model_Of_Interest__c[] tempSpo = new Model_Of_Interest__c[]{};
    if(pacctidsS.size() > 0){
       	tempSpo = [select Id from Model_Of_Interest__c where Account__c IN :pacctidsS AND RecordTypeId = '012900000000XAJAA2' and Category__c = 'Sport Interest']; 	
    	if(tempSpo.size() > 0){
    		delete tempSpo;
    	}
    }
    
	if(tempSport.size() > 0){
		insert tempSport;
	}

}