// Trigger to manage changes to Enform Usage Statistics records
// Author: Shabu, SP on 02.04.2013

trigger SP_EnformUsageStatisticsManagement on Enform_Usage_Statistics__c (before insert, before update, after insert) 
{
	/* 
		Before inserting/updating an Enform Usage Statistics, the mandatory fields should be validated.
	*/
	try
	{
		if (trigger.isBefore)
			SP_EnformUsageStatisticsManagement.doValidateEnformUsageStatistics(trigger.new); 
		else
			SP_EnformUsageStatisticsManagement.SetEnformUsageStatisticsForEnformUser(trigger.new);
	}
	catch(SPException ex) 
	{
		system.debug('####### Trigger SP_EnformUsageStatisticsManagement on Enform Usage Statistics Failed.');
		ex.Log();
		
		trigger.new[0].addError(ex.getMessage());
	}	
	catch(Exception ex)
	{
		system.debug('####### Trigger SP_EnformUsageStatisticsManagement on Enform Usage Statistics Failed.');
		SPException.LogSystemException(ex);
		
		trigger.new[0].addError(ex.getMessage());
	}
}