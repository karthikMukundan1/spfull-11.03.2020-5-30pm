trigger taskCountNumberOfActivity on Task (after delete, after insert) {
	//Task t = new Task();

	Id[] acctids = new Id[]{};
	Id[] pacctids = new Id[]{};
	List<String> tempLWKPIIds2 = new List<String>();
	String tmpWhatId = '';
	String strM = '';
	String strY = '';
	
	Warranty_KPI__c[] tempWKPI = new Warranty_KPI__c[]{};
	Pattern yPattern = Pattern.compile('[,/.]');
	
	if (Trigger.isInsert){
			
		for (Task t : Trigger.new) {
			if(t.WhatId != null){
				system.debug('Whatid = ' + t.WhatId );
				tmpWhatId = t.WhatId;		 
				if(tmpWhatId.startsWith('001')){
					acctids.add(t.WhatId);
				}
			}			 
		}
		
		if (acctids != null){		
			Account[] acctList = [Select Id, Name from Account 
			where Id in :acctids
			AND isPersonAccount = true];
			if (acctList != null){
				for(Account a : acctList) {
		   			pacctids.add(a.Id);	   			
				}	
			}
		}
		
		strM = date.today().month().format();
		strY = date.today().year().format();
	
		strY = yPattern.matcher(strY).replaceAll('');
		
		Map<String, Id> mapWarrantyKPIId = new Map<String, Id>();
		Map<String, Double> mapWarrantyKPINum = new Map<String, Double>();
		Id[] wkpiIddel = new Id[]{};
		
		if(pacctids != null){
			Warranty_KPI__c[]wkpiList = [Select Account__c, Id, Number_of_Activity__c From Warranty_KPI__c 
										  where Month__c = :strM and Year__c = :strY and Account__c IN :pacctids];
						
			if(wkpiList != null){
				for(Warranty_KPI__c w : wkpiList){			
					String tempwkpiId = '';
					tempwkpiId = w.Account__c + strM + strY;
					mapWarrantyKPIId.put(tempwkpiId,w.Id);
					mapWarrantyKPINum.put(tempwkpiId,w.Number_of_Activity__c);
				}	
			}
			
				for(Integer i = 0; i< pacctids.size(); i++){		
					Warranty_KPI__c wk = new Warranty_KPI__c();
					String tempWKPIId = pacctids[i] +  strM + strY;							
					Integer intNum = 0;		
					if (mapWarrantyKPIId.get(tempWKPIId) == null){			
					    wk.Account__c = pacctids[i];
					    wk.Month__c = strM;
					    wk.Year__c = strY;
					    wk.Number_of_Activity__c = 1;		        				        	
		        		tempWKPI.add(wk);				
					}else{
						wkpiIddel.add(mapWarrantyKPIId.get(tempWKPIId));
					}
				}					
		}
				
		Warranty_KPI__c[] tempW = new Warranty_KPI__c[]{};
	    if(wkpiIddel.size() > 0){
	       	tempW = [select Id, Number_of_Activity__c from Warranty_KPI__c where Id IN :wkpiIddel]; 	
	    	if(tempW.size() > 0){
	    		for(Warranty_KPI__c wkk : tempW){
	    			wkk.Number_of_Activity__c = wkk.Number_of_Activity__c + 1;
	    		}
	    		update tempW;
	    	}
	    }
	
		if(tempWKPI.size() > 0){
			insert tempWKPI;
		}			
	}else if (Trigger.isDelete){
	
		for (Task t : Trigger.old) {
			
			if(t.WhatId != null){
				tmpWhatId = t.WhatId;		 
				if(tmpWhatId.startsWith('001')){
					acctids.add(t.WhatId);
					strM = t.CreatedDate.month().format();
					strY = t.CreatedDate.year().format();
					strY = yPattern.matcher(strY).replaceAll('');
					String strtempWhatID = t.WhatId;
					strtempWhatID = strtempWhatID.substring(0,15);
					String tempwkpiId = strtempWhatID + strM + strY;
					tempLWKPIIds2.add(tempwkpiId);
					system.debug('AAAA = ' + tempwkpiId);			
				}
			}				 
		}
	 	
		Warranty_KPI__c[] tempW = new Warranty_KPI__c[]{};
	    if(tempLWKPIIds2.size() > 0){
	       	tempW = [select Id, Number_of_Activity__c, TriggerID__c from Warranty_KPI__c where Account__c IN :acctids]; 	
	    	if(tempW.size() > 0){
	    		for(Warranty_KPI__c wkk : tempW){
	    			for(Integer i = 0; i< tempLWKPIIds2.size(); i++){
	    				String strtID = wkk.TriggerID__c;
	    				if(strtID.equals(tempLWKPIIds2.get(i))){
	    					wkk.Number_of_Activity__c = wkk.Number_of_Activity__c - 1;
	    				}
	    			}	    			
	    		}
	    		update tempW;
	    	}
	    }	
	}

}