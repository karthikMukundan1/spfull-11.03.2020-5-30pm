/*******************************************************************************
@author:		Donnie Banez
@date:         	October 2018
@description:  	Trigger for Leads 
@Test Methods:	LeadTriggerHandlerTest 
*******************************************************************************/
trigger LeadTrigger on Lead (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Lead Trigger Handler 
    LeadTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}