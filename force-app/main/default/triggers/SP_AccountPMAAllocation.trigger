trigger SP_AccountPMAAllocation on Account (before insert, before update)
{
//	This trigger updates the PMA Dealer field based on the postcode

	set<string>			set_Postcodes			= new set<string>();
	map<string, string> map_PostcodetoDealer	= new map<string, string>();

	// Build a set of Postcodes in the trigger dataset
	for (Account sAccount : trigger.new)
	{
		if (	sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType() ||
				sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType()
			)
		{
			if (sAccount.ShippingPostalCode != null)
			{
				// if Shipping Postcode starts with a 0, then remove it.
				if (sAccount.ShippingPostalCode.startsWith('0')) 
				{
					set_Postcodes.add(sAccount.ShippingPostalCode.substring(1));
				}
				else
				{
					set_Postcodes.add(sAccount.ShippingPostalCode);
				}
			}

			if (sAccount.BillingPostalCode != null)
			{
				// if Billing Postcode starts with a 0, then remove it.
				if (sAccount.BillingPostalCode.startsWith('0')) 
				{
					set_Postcodes.add(sAccount.BillingPostalCode.substring(1));
				}
				else
				{
					set_Postcodes.add(sAccount.BillingPostalCode);
				}
			}
		}
	}

	// Now build a map of Dealer name based on the Account postcodes
	for (PMA__c [] arrPMA : [	select	Postcode__c,
										Dealer_Name__c
								from	PMA__c
								where	Postcode__c in :set_Postcodes
							])
	{
		for (PMA__C sPMA : arrPMA)
		{
			if (sPMA.Dealer_Name__c != null)
			{
				map_PostcodetoDealer.put(sPMA.Postcode__c, sPMA.Dealer_Name__c);
			}
		}
	}

	// Finally, find the Dealer for each Account based on Shipping postcode, then Billing postcode
	for (Account sAccount : trigger.new)
	{
		if (	sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType() ||
				sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType()
			)
		{
			string strDealerName = null;

			if (sAccount.ShippingPostalCode != null)
			{
				// if Shipping Postcode starts with a 0, then remove it.
				if (sAccount.ShippingPostalCode.startsWith('0')) 
				{
					strDealerName = map_PostcodetoDealer.get(sAccount.ShippingPostalCode.substring(1));
				}
				else
				{
					strDealerName = map_PostcodetoDealer.get(sAccount.ShippingPostalCode);
				}
			}
			else
			{
				// Shipping postcode was null - look for Billing postcode
				if (sAccount.BillingPostalCode != null)
				{
					// if Billing Postcode starts with a 0, then remove it.
					if (sAccount.BillingPostalCode.startsWith('0')) 
					{
						strDealerName = map_PostcodetoDealer.get(sAccount.BillingPostalCode.substring(1));
					}
					else
					{
						strDealerName = map_PostcodetoDealer.get(sAccount.BillingPostalCode);
					}
				}
			}

			// Update the Account field
			if (strDealerName != null)
			{
				sAccount.PMA_Dealer__c = strDealerName;
				
				if (trigger.isUpdate)
					SP_Utilities.AccountUpdatedByPMAllocationTrigger = true;
			}
		}
	}
}