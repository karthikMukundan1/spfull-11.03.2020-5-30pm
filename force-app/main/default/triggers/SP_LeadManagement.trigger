trigger SP_LeadManagement on Lead (after update, before delete)
{
	// Flag to check if this trigger is fired as a result of the updation of First Name, Last Name,
	// Email and/or mobile fields when Intelematics User updates the similar fields in Enform User	
	boolean leadUpdatedByEnformTrigger = SP_Utilities.LeadUpdatedByEnformTrigger;
	
	if (SP_Utilities.LeadUpdatedByEnformTrigger)
		SP_Utilities.LeadUpdatedByEnformTrigger = false;
			
	try
	{
		// Send notification to Intelematics whenever a non-Itelematics User makes changes to First Name, 
		// Last Name, Email and/or Mobile		
		if (UserInfo.getUserId() != SPCacheLexusEnformMetadata.getIntelematicsUser())
		{
			if (trigger.isUpdate)
				SP_LeadManagement.doSyncNotification_LeadToEnformUser(trigger.oldMap, trigger.newMap);
			else
				SP_LeadManagement.doSyncNotification_LeadToEnformUser(trigger.oldMap, 'delete'); 

		}
		// If this trigger is fired when an Intelematics User updates First Name, Last Name, Email and/or 
		// Mobile fields of Enform User, nothing to do. 
		// But, when an Intelematics User directly updates a lead record, it should be checked if the 
		// updated fields are alowed for updation by an Intelematics User. If an Intelematics User is not
		// supposed to edit any of the non-permitted field, an error should be thrown.		
		else if (trigger.isUpdate && !leadUpdatedByEnformTrigger)
		{
			set<String> set_FieldNames = new set<String>();
			set_FieldNames.add('phone');
			set_FieldNames.add('work_phone__c');
			set_FieldNames.add('fax');
			set_FieldNames.add('address');
			
			string strField = SP_Utilities.checkFieldChangeAllowed(trigger.newMap, trigger.oldMap, set_FieldNames);	
			
			if (strField != null)	
				trigger.new[0].addError(' An Intelematics User is not allowed to edit ' + strField + '.');
			
		}
	}
	catch(Exception ex)
	{
		system.debug('######## SYSTEM EXCEPTION: Trigger SP_LeadManagement on Lead Failed.]');
		SPException.LogSystemException(ex, SP_EnformUtilities.getAccountOwnerFromLead(trigger.oldMap.keySet()).get(trigger.old[0].Id),
			   						SP_EnformUtilities.getOwnerFromLead(trigger.oldMap.keySet()).get(trigger.old[0].Id));
		
		throw ex;
	}
}