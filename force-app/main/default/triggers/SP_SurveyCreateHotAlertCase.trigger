trigger SP_SurveyCreateHotAlertCase on Survey__c (after insert, after update)
{
	// This trigger will create a new Hot Alert Case for Potentiate Surveys marked as Hot Alerts

	// Set of related Account ids
	set<id>				set_AccountIds			= new set<id>();

	// Map of Accounts and Contacts
	map<id, Account>	map_Accounts			= new map<id, Account>();
	map<id, Contact>	map_Contacts			= new map<id, Contact>();

	// Cases to be created
	list<Case>			li_CasestoInsert	= new list<Case>();


	// Processing
	for (Survey__c sSurvey : trigger.new)
	{
		if (!sSurvey.Hot_Alert__c)
			continue;

		// For updates, only run this code if we are changing the Hot Alert flag from false to true
		if (trigger.isUpdate)
		{
			Survey__c sOldSurvey = trigger.oldMap.get(sSurvey.Id);
			
			if (!sOldSurvey.Hot_Alert__c && sSurvey.Hot_Alert__c)
			{
				// OK
			}
			else
			{
				continue;
			}
		}


		// Dealer are responsible for Hot Alerts assigned to them, we only want those assigned to Lexus Australia
		if (sSurvey.Hot_Alert_Responsibility__c == null || sSurvey.Hot_Alert_Responsibility__c.toUppercase() != 'LEXUS AUSTRALIA')
			continue;

		if (sSurvey.Customer__c != null)
			set_AccountIds.add(sSurvey.Customer__c);
	}

	for (Account [] arrAccounts :	[	select	Id,
												OwnerId

	    								from	Account
	    								where	Id in :set_AccountIds
	    								and		(	RecordTypeId = :SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() or
													RecordTypeId = :SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType()
												)
	    							])
	{
		for (Account sAccount : arrAccounts)
		{
			map_Accounts.put(sAccount.Id, sAccount);
		}
	}

	for (Contact [] arrContacts :	[	select	Id,
												AccountId

	    								from	Contact
	    								where	AccountId in :set_AccountIds
	    							])
	{
		for (Contact sContact : arrContacts)
		{
			map_Contacts.put(sContact.AccountId, sContact);
		}
	}


	for (Survey__c sSurvey : trigger.new)
	{
		if (!sSurvey.Hot_Alert__c)
			continue;

		// For updates, only run this code if we are changing the Hot Alert flag from false to true
		if (trigger.isUpdate)
		{
			Survey__c sOldSurvey = trigger.oldMap.get(sSurvey.Id);
			
			if (!sOldSurvey.Hot_Alert__c && sSurvey.Hot_Alert__c)
			{
				// OK
			}
			else
			{
				continue;
			}
		}

		if (sSurvey.Hot_Alert_Responsibility__c == null || sSurvey.Hot_Alert_Responsibility__c.toUppercase() != 'LEXUS AUSTRALIA')
			continue;

		Case sCase = new Case();

		sCase.RecordTypeId		= SPCachePotentiateMetadata.getHotAlertRecordTypeID();

		if (map_Contacts.containsKey(sSurvey.Customer__c))
		{
			sCase.ContactId			= map_Contacts.get(sSurvey.Customer__c).Id;
		}
		else
		{
			// This should never happen
		}

		if (sSurvey.Survey_Type__c == SPCachePotentiateMetadata.getSurveyTypeSales())
		{
			sCase.Type			= SPCachePotentiateMetadata.getCaseTypeSales();
		}
		else if (sSurvey.Survey_Type__c == SPCachePotentiateMetadata.getSurveyTypeService())
		{
			sCase.Type			= SPCachePotentiateMetadata.getCaseTypeService();
		}

		if (map_Accounts.containsKey(sSurvey.Customer__c))
		{
			sCase.OwnerId			= map_Accounts.get(sSurvey.Customer__c).OwnerId;
		}
		else
		{
			// Could not find the Account - assign this Case to the default owner (usually system admin)
			sCase.OwnerId			= SPCachePotentiateMetadata.getCaseDefaultOwner();
		}

		sCase.Status			= 'In Progress';
		sCase.Origin			= SPCachePotentiateMetadata.getCaseOrigin();
		sCase.Subject			= 'Survey Hot Alert';
		sCase.Description		= sSurvey.Hot_Alert_Transcript__c;

		li_CasesToInsert.add(sCase);
	}

	// Create new Teaks
	if (!li_CasesToInsert.isEmpty())
	{
		insert li_CasesToInsert;
	}

}