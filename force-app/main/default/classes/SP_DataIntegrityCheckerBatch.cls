global with sharing class SP_DataIntegrityCheckerBatch implements Database.Batchable<sObject>
{
	public class SP_DataIntegrityCheckerBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		CSV_Header__c sCSVHeader = [	select	id
										from	CSV_Header__c
										where	Processed__c = false
										limit	1
									];

		string	strQuery =		
			' select	Id'	+
			' from 		CSV_Data__c' +
			' where 	CSV_Header__c = ' + '\'' + sCSVHeader.Id + '\'' +
			' limit 20000';

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

//		system.debug('*** theProcessList ***' + theProcessList);

		SP_DataIntegrityCheckerProcessing.SP_Args oArgs = new SP_DataIntegrityCheckerProcessing.SP_Args(theProcessList);

		SP_DataIntegrityCheckerProcessing processor = new SP_DataIntegrityCheckerProcessing(oArgs);
		SP_DataIntegrityCheckerProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		CSV_Header__c sCSVHeader = [	select	id,
												Processed__c
										from	CSV_Header__c
										where	Processed__c = false
										limit	1
									];

		sCSVHeader.Processed__c = true;
		
		update sCSVHeader;

		system.debug('Finished');

//		string strSchedule = '0 30 * * * ?';

//		System.Schedule('SP_DataIntegrityCheckerSchedule', strSchedule, new SP_DataIntegrityCheckerSchedule());

	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{

	}
}