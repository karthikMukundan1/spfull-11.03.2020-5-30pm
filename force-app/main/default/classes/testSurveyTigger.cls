/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testSurveyTigger {

    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.RecordTypeId = '012900000000THw';
        acc.Type = 'Customer';
        acc.LastName = 'Last Name';
        acc.BillingPostalCode = '2000';
        acc.PersonMobilePhone = '0499990000';
        acc.FirstName = 'First Name';
        acc.Salutation = 'Mr.';
        insert acc;
        
        System.assertEquals(acc.PersonContactId, null);
        
        //Service
        Survey__c survey1 = new Survey__c();
        survey1.Survey_Type__c = 'Service';
        survey1.Contact_Code__c = 'Completed';
        survey1.Survey_Date__c = System.today();
        survey1.Customer__c = acc.Id;
        insert survey1;
        
        /*Survey__c survey2 = new Survey__c();
        survey2.Survey_Type__c = 'Service';
        survey2.Contact_Code__c = 'No Marketing';
        survey2.Survey_Date__c = System.today();
        survey2.Customer__c = acc.Id;
        insert survey2;*/
        
        Survey__c survey3 = new Survey__c();
        survey3.Survey_Type__c = 'Service';
        survey3.Contact_Code__c = 'No Survey';
        survey3.Survey_Date__c = System.today();
        survey3.Customer__c = acc.Id;
        insert survey3;
        
        Survey__c survey4 = new Survey__c();
        survey4.Survey_Type__c = 'Service';
        survey4.Contact_Code__c = 'Deceased';
        survey4.Survey_Date__c = System.today();
        survey4.Customer__c = acc.Id;
        insert survey4;
        
        Survey__c survey5 = new Survey__c();
        survey5.Survey_Type__c = 'Service';
        survey5.Contact_Code__c = 'Unserviced';
        survey5.Survey_Date__c = System.today();
        survey5.Customer__c = acc.Id;
        insert survey5;
        
        //Sales
        Survey__c survey6 = new Survey__c();
        survey6.Survey_Type__c = 'Sales';
        survey6.Contact_Code__c = 'Completed';
        survey6.Survey_Date__c = System.today();
        survey6.Customer__c = acc.Id;
        insert survey6;
        
        /*Survey__c survey7 = new Survey__c();
        survey7.Survey_Type__c = 'Sales';
        survey7.Contact_Code__c = 'No Marketing';
        survey7.Survey_Date__c = System.today();
        survey7.Customer__c = acc.Id;
        insert survey7;*/
        
        Survey__c survey8 = new Survey__c();
        survey8.Survey_Type__c = 'Sales';
        survey8.Contact_Code__c = 'No Survey';
        survey8.Survey_Date__c = System.today();
        survey8.Customer__c = acc.Id;
        insert survey8;
        
        Survey__c survey9 = new Survey__c();
        survey9.Survey_Type__c = 'Sales';
        survey9.Contact_Code__c = 'Deceased';
        survey9.Survey_Date__c = System.today();
        survey9.Customer__c = acc.Id;
        insert survey9;
        
        Survey__c survey10 = new Survey__c();
        survey10.Survey_Type__c = 'Sales';
        survey10.Contact_Code__c = 'Incorrect Number';
        survey10.Survey_Date__c = System.today();
        survey10.Customer__c = acc.Id;
        insert survey10;
    }
}