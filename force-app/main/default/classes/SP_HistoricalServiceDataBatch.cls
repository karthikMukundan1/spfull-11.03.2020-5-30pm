global with sharing class SP_HistoricalServiceDataBatch implements Database.Batchable<sObject>
{
	public class SP_HistoricalServiceDataBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery =	
	
			' select	Id'	+
			' from 		Account' +
			' where 	Service_Value__c = null ' +
			' and		RecordTypeId = \'' + SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() + '\'' +
			'limit 20000';
			
		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

//		system.debug('*** theProcessList ***' + theProcessList);

		SP_HistoricalServiceDataProcessing.SP_Args oArgs = new SP_HistoricalServiceDataProcessing.SP_Args(theProcessList);

		SP_HistoricalServiceDataProcessing processor = new SP_HistoricalServiceDataProcessing(oArgs);
		SP_HistoricalServiceDataProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');

//		string strSchedule = '0 30 * * * ?';

//		System.Schedule('SP_HistoricalServiceDataSchedule', strSchedule, new SP_HistoricalServiceDataSchedule());

	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{

	}
}