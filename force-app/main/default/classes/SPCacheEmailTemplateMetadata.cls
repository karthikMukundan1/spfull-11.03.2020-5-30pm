public with sharing class SPCacheEmailTemplateMetadata
{
    private static SPCacheEmailTemplateMetadata__c CacheEmailTemplateMetadata;
    
    /***********************************************************************************************************
        Lookup Cached information
    ***********************************************************************************************************/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    public static string getCaseTypeAccessoriesEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeAccessoriesEnquiry__c;
    }

    public static string getCaseTypeBuildandPriceEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeBuildandPriceEnquiry__c;
    }

    public static string getCaseTypeGeneralEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeGeneralEnquiry__c;
    }

    public static string getCaseTypePersonalEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypePersonalEnquiry__c;
    }

    public static string getCaseTypeNewVehicleInterest()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeNewVehicleInterest__c;
    }
    
    public static string getCaseTypeConciergeOffer()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeConciergeOffer__c;
    }
    
    public static string getCaseTypeDomPerignonOffer()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeDomPerignonOffer__c;
    }   

    public static string getTemplateIdAccessoriesEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIdAccessoriesEnquiry__c;
    }

    public static string getTemplateIdBuildandPriceEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIdBuildandPriceEnquiry__c;
    }

    public static string getTemplateIdGeneralEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIdGeneralEnquiry__c;
    }

    public static string getTemplateIdPersonalEnquiry()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIdPersonalEnquiry__c;
    }

    public static string getTemplateIdNewVehicleInterest()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIdNewVehicleInterest__c;
    }
    
    public static string getTemplateIdTemplateIDConciergeOffer()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIDConciergeOffer__c;
    }   
    
    public static string getTemplateIdTemplateIDDomPerignonOffer()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIDDomPerignonOffer__c;
    }       
    
    public static string getConciergeOfferEmail()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.ConciergeOfferEmail__c;
    }   
    
    public static string getConciergeEmailContactID()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.ConciergeEmailContactID__c;
    }               

    public static string getCaseTypeNXPreLaunch()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeNXPreLaunch__c;
    } 
      
   public static string getTemplateIdTemplateIDNXPreLaunch()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIDNXPreLaunch__c;
    }

// 4 new case types for mass email
   public static string getTemplateIdMassEmail()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.TemplateIdMassEmail__c;
    }

	
	//Vehicle Information
    public static string getCaseTypeVehicleInformation()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeVehicleInformation__c;
    }
    
	//Parts, Services, Warranty 
    public static string getCaseTypePartsServicesWarranty()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypePartsServicesWarranty__c;
    }    

	//Lexus Financial Services
    public static string getCaseTypeLexusFinancialServices()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeLexusFinancialServices__c;
    }    

	//Feedback
    public static string getCaseTypeFeedback()
    {
        if (CacheEmailTemplateMetadata == null)
            LoadMetadata(); 
            
        return CacheEmailTemplateMetadata.CaseTypeFeedback__c;
    } 
    
        
    /***********************************************************************************************************
        Worker Methods
    ***********************************************************************************************************/
    private static void LoadMetadata()
    {
        CacheEmailTemplateMetadata = SPCacheEmailTemplateMetadata__c.getInstance();
    }
    
    /***********************************************************************************************************
        Test Methods
    ***********************************************************************************************************/
    public static testmethod void TestSPCacheEmailTemplateMetadata()
    {
        SPCacheEmailTemplateMetadata.getCaseTypeAccessoriesEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getCaseTypeBuildandPriceEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getCaseTypeGeneralEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getCaseTypePersonalEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getCaseTypeNewVehicleInterest();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getTemplateIdAccessoriesEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getTemplateIdBuildandPriceEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getTemplateIdGeneralEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getTemplateIdPersonalEnquiry();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getTemplateIdNewVehicleInterest();
        CacheEmailTemplateMetadata = null;

        
        SPCacheEmailTemplateMetadata.getTemplateIdMassEmail();
        CacheEmailTemplateMetadata = null;        

        SPCacheEmailTemplateMetadata.getCaseTypeVehicleInformation();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getCaseTypePartsServicesWarranty();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getCaseTypeLexusFinancialServices();
        CacheEmailTemplateMetadata = null;

        SPCacheEmailTemplateMetadata.getCaseTypeFeedback();
        CacheEmailTemplateMetadata = null;        
    }
}