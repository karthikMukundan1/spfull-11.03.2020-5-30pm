// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class VehicleOwnershipTriggerHandlerTest {
	static Account partnerVehicle;
    static Account partnerCustomer;
    static Offer__c vehicleOffer;
    static Offer__c customerOffer;
    static Vehicle_Ownership__c validForOfferVO;
    static Asset__c asset;
    static Account customer;
    
    static testmethod void testCreateVOOfferDetail(){
        dataSetup();
        // creation of VO
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        validForOfferVO.Status__c = 'Inactive';
        update validForOfferVO;
        validForOfferVO.Status__c = 'Active';
        update validForOfferVO;
        List<VO_Offer_Detail__c> voodList = [select Id, CustomerOfferDetail__c, Offer__c, Offer__r.Offer_Class__c from VO_Offer_Detail__c 
                                                  where Vehicle_Ownership__c = :validForOfferVO.Id
                                             and (Offer__c = :vehicleOffer.Id or Offer__c = :customerOffer.Id) // only filtering based on test records
                                                 ];
        
        List<Customer_Offer_Detail__c> coodList = [select Id, Customer__c, Offer__c from Customer_Offer_Detail__c 
                                                  where Offer__c = :customerOffer.Id
                                                   and Customer__c = :customer.Id];
        
    }
    
    
    static testmethod void testCreateCOOfferDetail(){
        dataSetup();
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        validForOfferVO.Status__c = 'Inactive';
        update validForOfferVO;
        validForOfferVO.Status__c = 'Active';
        update validForOfferVO;
        List<Customer_Offer_Detail__c> coodList = [select Id, Offer__c, First_Credit_Expiration__c, Offer__r.Offer_Class__c,
                                                   Total_Credits_Allowed__c from Customer_Offer_Detail__c 
                                                  where Customer__c = :customer.Id
                                             and (Offer__c = :vehicleOffer.Id or Offer__c = :customerOffer.Id) // only filtering based on test records
                                                 ];
        
        List<VO_Offer_Detail__c> voodList = [select Id, CustomerOfferDetail__c, CustomerOfferDetail__r.First_Credit_Expiration__c, Vehicle_Ownership__r.Valet_Voucher_Expiry_Date__c, Offer__c, Offer__r.Offer_Class__c from VO_Offer_Detail__c 
                                                  where Vehicle_Ownership__c = :validForOfferVO.Id
                                             and (Offer__c = :vehicleOffer.Id or Offer__c = :customerOffer.Id) // only filtering based on test records
                                                 ];
        
    }
    
    static testmethod void testVODeletion(){
    	dataSetup();
        validForOfferVO.Magic_Link_Sent__c = true;
        update validForOfferVO;
        try {
            delete validForOfferVO;
        }
        catch (DmlException e){
            system.assertEquals(true, e.getMessage().contains(Label.VODeletionError)); // has error
        }
        
    }
    
    static testmethod void testCodeCompensation(){
        VehicleOwnershipTriggerHandler.codeCompensation();
    }
    
    static void dataSetup(){
        Test.setMock(HttpCalloutMock.class, new MockDerivedLinkHttpRespGenerator());
        List<Account> newAcctList = new List<Account>();
        List<Offer__c> offerList = new List<Offer__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        partnerVehicle = new Account(Name = 'Partner Vehicle', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Vehicle');
        partnerCustomer = new Account(Name = 'Partner Customer', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerVehicle);
        newAcctList.add(partnerCustomer);
        insert newAcctList;
        validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        customerOffer = new Offer__c(Status__c = 'Inactive');
        customerOffer.Offer_Class__c = 'Customer';
        customerOffer.Offer_Type__c = 'Car Hire';
        customerOffer.Customer_Platinum_Limit__c = 10;
        customerOffer.Partner_Account__c = partnerCustomer.Id;
        insert customerOffer;
        customerOffer.Status__c = 'Active';
        update customerOffer;
        
        vehicleOffer = new Offer__c(Status__c = 'Inactive');
        vehicleOffer.Offer_Class__c = 'Vehicle';
        vehicleOffer.Offer_Type__c = 'Valet Service';
        vehicleOffer.Vehicle_Offer_Platinum_Limit__c = 10;
        vehicleOffer.Partner_Account__c = partnerVehicle.Id;
        insert vehicleOffer;
        vehicleOffer.Status__c = 'Active';
        update vehicleOffer;
        
        
    }
}