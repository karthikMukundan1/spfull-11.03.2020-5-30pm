// D BANEZ Jan 2020 - temporary test methods to deploy changes to NewWarrantyUpdateAccountTest trigger on Warranty__c (for De Rdr Project)
@IsTest (SeeAllData=true)
public class NewWarrantyUpdateAccountTest {
    static testmethod void testNewWarranty(){
        // setup data
        Warranty__c w = [select Id, Warranty_Type__c, Expiration_Date__c from Warranty__c where Warranty_Type__c = 'Factory Warranty' 
                         and Expiration_Date__c != null limit 1];
        w.Expiration_Date__c = w.Expiration_Date__c + 10;
        update w;
    }
}