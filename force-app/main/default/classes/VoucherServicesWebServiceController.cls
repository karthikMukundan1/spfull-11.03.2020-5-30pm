/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Voucher Services WS controller to allow LCAC to call the Voucher Services Web Service methods
 * 					
 * VF PAGE USING THIS CLASS: MyLexusWSTestPage
 * TEST CLASS: MyLexusWebServicesControllerTest
 * */
public with sharing class VoucherServicesWebServiceController {
    public String username {get;set;}
    public String password {get;set;}
    public String partnerBranchIdentifier {get;set;}
    public String partnerBranchId {get;set;}
    public String vOOfferDetailID {get;set;} 
    public Boolean compFlag {get;set;}
    public String compComment {get;set;}
    public String vcPartnerBranchId {get;set;}
    public String regoNumber {get;set;} 
    public String inputErrCode {get;set;}
    public Voucher__c redeemVoucher {get;set;}
    public Voucher__c vcVoucher {get;set;}
    
    public VoucherServicesWebService.VoucherServiceResult valetVoucherLoginResponse {get;set;}
    public VoucherServicesWebService.VoucherServiceResult valetPasswordResendResponse {get;set;}
    public VoucherServicesWebService.VoucherServiceResult valetRedeemVoucher {get;set;}
    public List<VoucherServicesWebService.VoucherAvailabilityResult> voucherAvailabilityList {get;set;}
        
    public VoucherServicesWebServiceController() {
        voucherAvailabilityList = new List<VoucherServicesWebService.VoucherAvailabilityResult>();
        valetVoucherLoginResponse = new VoucherServicesWebService.VoucherServiceResult();
        valetPasswordResendResponse = new VoucherServicesWebService.VoucherServiceResult();
        valetRedeemVoucher = new VoucherServicesWebService.VoucherServiceResult(); 
        Id VOVOUCHERRTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('VO_Voucher').getRecordTypeId();
        redeemVoucher = new Voucher__c(RecordTypeId = VOVOUCHERRTYPEID);
        vcVoucher = new Voucher__c(RecordTypeId = VOVOUCHERRTYPEID);
    }
    
    public PageReference attendantLogin(){
        valetVoucherLoginResponse = VoucherServicesWebService.attendantLogin(username, password);
        return null;
    }
    
    public PageReference valetPasswordResend(){
        valetPasswordResendResponse = VoucherServicesWebService.resendPassword(partnerBranchIdentifier);
        return null;
    }
    
    public PageReference redeemVoucher(){
        Boolean validId = true;
        String errMessage = '';
        if (partnerBranchId == '') partnerBranchId = null;
        if (vOOfferDetailID == '') vOOfferDetailID = null;
        
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (partnerBranchId != null){
            validId = Pattern.matches(idRegex, partnerBranchId);
            errMessage = 'Invalid Partner Branch ID format';
            
        }
        if (vOOfferDetailID != null){
            validId = Pattern.matches(idRegex, vOOfferDetailID);
            errMessage += '; Invalid VO Offer Detail ID format;';
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) valetRedeemVoucher = VoucherServicesWebService.redeemVoucher(partnerBranchId, redeemVoucher.Voucher_Type__c, vOOfferDetailID, compFlag, compComment, inputErrCode);
        return null;
    }
    
    public PageReference getVoucherAvailability(){
        Boolean validId = true;
        String errMessage = '';
        if (vcPartnerBranchId == '') vcPartnerBranchId = null;
        
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (vcPartnerBranchId != null){
            validId = Pattern.matches(idRegex, vcPartnerBranchId);
            errMessage = 'Invalid Partner Branch ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) voucherAvailabilityList = VoucherServicesWebService.getVoucherAvailability(vcPartnerBranchId, regoNumber, vcVoucher.Voucher_Type__c);
        return null;
    }
}