public with sharing class SP_ServicedYearProcessing
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	private static integer iQueryLimit = 200;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	list<Asset__c>			li_Assets					= new list<Asset__c>();

	// Map of Asset Ids to a set of Serviced Years
	map<id, set<string>>	map_AssettoServicedYear		= new map<id, set<string>>();

	list<Asset__c>			li_AssetstoUpdate			= new list<Asset__c>();
		
	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_ServicedYearProcessing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		Savepoint savePoint;
		
		if(mArgs.UseSavePoint)
			savePoint = Database.setSavepoint();
		
		try
		{
			ProcessAssetObject(mArgs.ProcessList);
		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
						
			if(savePoint != null)
				Database.rollback(savePoint);
			
			throw new SP_Exception('Fatal Exception - All changes have been rolled back - ' + e.getMessage());
		}
		
		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Process Asset Objects
	private void ProcessAssetObject(list<id> li_AssetIdsIn)
	{	
		// Get all the Asset records
		Asset__c [] li_Assets =	[	select	id,
											Serviced_In_Year__c
									from	Asset__c
									where	Id in :li_AssetIdsIn
								];


		// Get all the Service records for these Assets
		for (Service__c [] arrServices :	[	select	id,
														Asset__c,
														Service_Date__c
												from	Service__c
												where	Asset__c in :li_AssetIdsIn
											])
		{
			for (Service__c sService : arrServices)
			{
				if (sService.Asset__c != null && sService.Service_Date__c != null)
				{
					string strServicedYear = sService.Service_Date__c.year().format();
					strServicedYear = strServicedYear.replaceAll(',', '');

					if (!map_AssettoServicedYear.containsKey(sService.Asset__c))
					{
						set<string> set_ServicedYear = new set<string>();
						set_ServicedYear.add(strServicedYear);
						map_AssettoServicedYear.put(sService.Asset__c, set_ServicedYear);
					}
					else
					{
						set<string> set_ServicedYear = map_AssettoServicedYear.get(sService.Asset__c);
						set_ServicedYear.add(strServicedYear);
						map_AssettoServicedYear.put(sService.Asset__c, set_ServicedYear);
					}
				}
			}
		}
		
		for (Asset__c sAsset : li_Assets)
		{
			if (map_AssettoServicedYear.containsKey(sAsset.Id))
			{
				set<string> set_ServicedYear = map_AssettoServicedYear.get(sAsset.Id);
				list<string> li_ServicedYear = new list<string>();
	
				for (string strServicedYear : set_ServicedYear)
				{
					li_ServicedYear.add(strServicedYear);
				}
	
				li_ServicedYear.sort();
	
				sAsset.Serviced_In_Year__c = '';
				
				for (string strServicedYear : li_ServicedYear)
				{
					sAsset.Serviced_In_Year__c += strServicedYear;
					sAsset.Serviced_In_Year__c += ', ';
				}
	
				// Back up one place to remove the trailing space
				sAsset.Serviced_In_Year__c = sAsset.Serviced_In_Year__c.substring(0, sAsset.Serviced_In_Year__c.length() - 1);
			}

			li_AssetstoUpdate.add(sAsset);
		}

		if (!li_AssetstoUpdate.isEmpty())
		{
			update li_AssetstoUpdate;
		}

	}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_ServicedYearProcessing()
	{

	}
}