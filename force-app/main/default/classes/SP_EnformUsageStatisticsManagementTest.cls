@isTest
public class SP_EnformUsageStatisticsManagementTest 
{
	private static Enform_User__c CreateEnformUser()
	{
		Account sAccountWithEU = new Account();
		sAccountWithEU.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccountWithEU.LastName				= 'TestLast1';
		sAccountWithEU.PersonEmail			= 'test1@email.com';
		
		insert sAccountWithEU;
		
		Enform_User__c testEU = new Enform_User__c(Owner_Id__c = sAccountWithEU.Id,
												   Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last',
												   IsPrimaryUser__c = true);
		insert testEU;
		
		return 	testEU;	
	}
	
	// Insert Enform Usage Statistics - All Mandatory fields filled
	
	// Result
	
	// Enform Usage statistics record inserted
	@isTest(SeeAllData=true)
    public static void TestCase001() 
    {
    	Test.startTest();
    	
    	Enform_User__c TestEU = CreateEnformUser();
    	
 		Enform_Usage_Statistics__c testEUStat = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id, 
																				Statistics_Period__c = 'Weekly',
																				Enform_App__c = 'xx',
																				Number_of_App_Launches__c = 1,
																				Statistics_Period_Ending__c = Date.today());
		insert testEUStat;   
		
		Test.stopTest();
		
		system.assertNotEquals(testEUStat.Id, null);   	
    	
    }
    
	// Upadte Enform Usage Statistics - Some Mandatory fields missed
	
	// Result
	
	// Enform Usage statistics record inserted
	@isTest(SeeAllData=true)
    public static void TestCase002() 
    {
    	Enform_User__c TestEU = CreateEnformUser();
    	
 		Enform_Usage_Statistics__c testEUStat = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id, 
																				Statistics_Period__c = 'Weekly',
																				Enform_App__c = 'xx',
																				Number_of_App_Launches__c = 1,
																				Statistics_Period_Ending__c = Date.today());
		insert testEUStat;   
		
    	Test.startTest();
    	
    	try
    	{
			testEUStat.Number_of_App_Launches__c = -1;
			update testEUStat; 		
    	}
    	catch (Exception ex)
    	{
    		system.debug('########## UPDATE FAILED');
    	}
		
		Test.stopTest();
		
		testEUStat = [select Number_of_App_Launches__c from Enform_Usage_Statistics__c where Id  = :testEUStat.Id];
		
		system.assertNotEquals(testEUStat.Number_of_App_Launches__c, -1);   	
    	
    }    
    
    // Insert Enform Usage Statistics - All Mandatory fields missed
	
	// Result
	
	// Enform Usage statistics record inserted
	@isTest(SeeAllData=true)
    public static void TestCase003() 
    {
    	Enform_User__c TestEU = CreateEnformUser();
    	
    	Test.startTest();
    	
 		Enform_Usage_Statistics__c testEUStat = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'none',
																				Statistics_Period_Ending__c = Date.today());
    	try
    	{
			insert testEUStat; 		
    	}
    	catch (Exception ex)
    	{
    		system.debug('########## INSERT FAILED');
    	}
		
		Test.stopTest();
		
		system.assertEquals(testEUStat.Id, null);   	
    	
    }  
    
     // Insert Enform Usage Statistics - All Mandatory fields missed
	
	// Result
	
	// Enform Usage statistics record inserted
	@isTest(SeeAllData=true)
    public static void TestCase004() 
    {
    	
    	
    	Test.startTest();
    	
    	Enform_User__c TestEU = CreateEnformUser();
    	
    	List<Enform_Usage_Statistics__c> testEUStats = new List<Enform_Usage_Statistics__c>();
    	
 		Enform_Usage_Statistics__c testEUStat_Concierge1 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Concierge',
 																				Number_of_App_Launches__c = 10,
																				Statistics_Period_Ending__c = system.today().addMonths(-4));
 		Enform_Usage_Statistics__c testEUStat_Concierge2 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Concierge',
 																				Number_of_App_Launches__c = 5,
																				Statistics_Period_Ending__c = system.today().addMonths(-2));
		Enform_Usage_Statistics__c testEUStat_Concierge3 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Concierge',
 																				Number_of_App_Launches__c = 6,
																				Statistics_Period_Ending__c = system.today());
																				
		testEUStats.add(testEUStat_Concierge1);
		testEUStats.add(testEUStat_Concierge2);
		testEUStats.add(testEUStat_Concierge3);

    	try
    	{
			insert testEUStats; 		
    	}
    	catch (Exception ex)
    	{
    		system.debug('########## INSERT FAILED');
    	}
    	
    	Test.stopTest();
    }    

	@isTest(SeeAllData=true)
    public static void TestCase005() 
    {
    	
    	
    	Test.startTest();
    	
    	Enform_User__c TestEU = CreateEnformUser();
    	
    	List<Enform_Usage_Statistics__c> testEUStats = new List<Enform_Usage_Statistics__c>();
    	
 		Enform_Usage_Statistics__c testEUStat_FuelFinder1 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Fuel Finder',
 																				Number_of_App_Launches__c = 10,
																				Statistics_Period_Ending__c = system.today().addMonths(-4));
 		Enform_Usage_Statistics__c testEUStat_FuelFinder2 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Fuel Finder',
 																				Number_of_App_Launches__c = 5,
																				Statistics_Period_Ending__c = system.today().addMonths(-2));
		Enform_Usage_Statistics__c testEUStat_FuelFinder3 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Fuel Finder',
 																				Number_of_App_Launches__c = 6,
																				Statistics_Period_Ending__c = system.today());
																				
		testEUStats.add(testEUStat_FuelFinder1);
		testEUStats.add(testEUStat_FuelFinder2);
		testEUStats.add(testEUStat_FuelFinder3);

    	try
    	{
			insert testEUStats; 		
    	}
    	catch (Exception ex)
    	{
    		system.debug('########## INSERT FAILED');
    	}
    	
    	Test.stopTest();
    } 
    
	@isTest(SeeAllData=true)
    public static void TestCase006() 
    {
    	
    	
    	Test.startTest();
    	
    	Enform_User__c TestEU = CreateEnformUser();
    	
    	List<Enform_Usage_Statistics__c> testEUStats = new List<Enform_Usage_Statistics__c>();
    	
 		Enform_Usage_Statistics__c testEUStat_LocalSearch1 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Local Search',
 																				Number_of_App_Launches__c = 10,
																				Statistics_Period_Ending__c = system.today().addMonths(-4));
 		Enform_Usage_Statistics__c testEUStat_LocalSearch2 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Local Search',
 																				Number_of_App_Launches__c = 5,
																				Statistics_Period_Ending__c = system.today().addMonths(-2));
		Enform_Usage_Statistics__c testEUStat_LocalSearch3 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Local Search',
 																				Number_of_App_Launches__c = 6,
																				Statistics_Period_Ending__c = system.today());
																				
		testEUStats.add(testEUStat_LocalSearch1);
		testEUStats.add(testEUStat_LocalSearch2);
		testEUStats.add(testEUStat_LocalSearch3);

    	try
    	{
			insert testEUStats; 		
    	}
    	catch (Exception ex)
    	{
    		system.debug('########## INSERT FAILED');
    	}
    	
    	Test.stopTest();
    }  
    
 	@isTest(SeeAllData=true)
    public static void TestCase007() 
    {
    	
    	
    	Test.startTest();
    	
    	Enform_User__c TestEU = CreateEnformUser();
    	
    	List<Enform_Usage_Statistics__c> testEUStats = new List<Enform_Usage_Statistics__c>();
    	
 		Enform_Usage_Statistics__c testEUStat_Traffic1 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Traffic',
 																				Number_of_App_Launches__c = 10,
																				Statistics_Period_Ending__c = system.today().addMonths(-4));
 		Enform_Usage_Statistics__c testEUStat_Traffic2 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Traffic',
 																				Number_of_App_Launches__c = 5,
																				Statistics_Period_Ending__c = system.today().addMonths(-2));
		Enform_Usage_Statistics__c testEUStat_Traffic3 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Traffic',
 																				Number_of_App_Launches__c = 6,
																				Statistics_Period_Ending__c = system.today());
																				
		testEUStats.add(testEUStat_Traffic1);
		testEUStats.add(testEUStat_Traffic2);
		testEUStats.add(testEUStat_Traffic3);

    	try
    	{
			insert testEUStats; 		
    	}
    	catch (Exception ex)
    	{
    		system.debug('########## INSERT FAILED');
    	}
    	
    	Test.stopTest();
    }   
    
	@isTest(SeeAllData=true)
    public static void TestCase008() 
    {
    	
    	
    	Test.startTest();
    	
    	Enform_User__c TestEU = CreateEnformUser();
    	
    	List<Enform_Usage_Statistics__c> testEUStats = new List<Enform_Usage_Statistics__c>();
    	
 		Enform_Usage_Statistics__c testEUStat_Weather1 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Weather',
 																				Number_of_App_Launches__c = 10,
																				Statistics_Period_Ending__c = system.today().addMonths(-4));
 		Enform_Usage_Statistics__c testEUStat_Weather2 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Weather',
 																				Number_of_App_Launches__c = 5,
																				Statistics_Period_Ending__c = system.today().addMonths(-2));
		Enform_Usage_Statistics__c testEUStat_Weather3 = new Enform_Usage_Statistics__c(Enform_User__c = TestEU.Id,
 																				Statistics_Period__c = 'Weekly',
 																				Enform_App__c = 'Weather',
 																				Number_of_App_Launches__c = 6,
																				Statistics_Period_Ending__c = system.today());
																				
		testEUStats.add(testEUStat_Weather1);
		testEUStats.add(testEUStat_Weather2);
		testEUStats.add(testEUStat_Weather3);

    	try
    	{
			insert testEUStats; 		
    	}
    	catch (Exception ex)
    	{
    		system.debug('########## INSERT FAILED');
    	}
    	
    	Test.stopTest();
    }                    
}