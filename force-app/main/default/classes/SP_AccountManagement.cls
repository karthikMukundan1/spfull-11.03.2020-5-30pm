public class SP_AccountManagement
{
	
	public static set<id> set_ChangedAccountIds	= new set<id>();
	public static set<id> set_ChangedEnformUserIds	= new set<id>(); 
	
/*
	If an Account has an Enform User record and changes have been made to fields that are synchronised between the
	two objects, and that change has not been made by the Intelematics userid, we need to call a future method in 
	SP_EnformUtilities to pass the Salesforce database ID of the updated Enform User record(s) to Intelematics 
	via a custom web service
*/
	public static void doSyncNotification_AccounttoEnformUser(map<id, Account> oldMap, map<id, Account> newMap)
	{
		map<ID, Account> map_OwnerToAccount = new map<ID, Account>();
		// First look for changed fields on an Owner record
		for (Account sOldAccount : oldMap.values())
		{
			if (sOldAccount.RecordTypeId != SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
			{
				system.debug('######## Account ' + sOldAccount.Id + ' is not Person Account Owner.');
				continue;
			}
			
			Account sNewAccount = newMap.get(sOldAccount.Id);
			
			system.debug('###### NEW ' + sNewAccount.LastName + ' ########## OLD ' + sOldAccount.LastName);
			
			if	(	sOldAccount.FirstName != sNewAccount.FirstName ||
					sOldAccount.LastName != sNewAccount.LastName ||
					sOldAccount.PersonMobilePhone != sNewAccount.PersonMobilePhone ||
					sOldAccount.PersonEmail != sNewAccount.PersonEmail ||
					sOldAccount.Customer_Password__pc != sNewAccount.Customer_Password__pc
				)
			{
				set_ChangedAccountIds.add(sOldAccount.Id);
				
				//if (sOldAccount.Customer_Password__pc != sNewAccount.Customer_Password__pc)
				map_OwnerToAccount.put(sNewAccount.Id, sNewAccount);
			}
		}
		
		if (set_ChangedAccountIds.isEmpty())
		{
			system.debug('######## There is no Accounts changed by a non-Intelematics User');
			return;
		}
		
		List<Enform_User__c> li_PrimaryEnformUsersUpd = new List<Enform_User__c>();
		
		// Now look for Enform Users
		for (Enform_User__c [] arrEnformUsers :	[	select	id, Owner_Id__c
													
													
													from	Enform_User__c
													where	Owner_Id__c in :set_ChangedAccountIds and
													IsPrimaryUser__c = true
												])
		{
			for (Enform_User__c sEnformUser : arrEnformUsers)
			{
				set_ChangedEnformUserIds.add(sEnformUser.Id);
				//if (map_OwnerToPassword.containsKey(sEnformUser.Owner_Id__c))
				//{
					sEnformUser.First_Name__c = map_OwnerToAccount.get(sEnformUser.Owner_Id__c).FirstName;
					sEnformUser.Last_Name__c = map_OwnerToAccount.get(sEnformUser.Owner_Id__c).LastName;
					sEnformUser.Mobile_Phone__c = map_OwnerToAccount.get(sEnformUser.Owner_Id__c).PersonMobilePhone;
					sEnformUser.Email__c = map_OwnerToAccount.get(sEnformUser.Owner_Id__c).PersonEmail;
					sEnformUser.Password__c = map_OwnerToAccount.get(sEnformUser.Owner_Id__c).Customer_Password__pc;
					
					li_PrimaryEnformUsersUpd.add(sEnformUser);
				//}
			}
		}
		
		if (set_ChangedEnformUserIds.isEmpty())
		{
			system.debug('######## There is no Enform Users associated with Accounts changed by the non-Intelematics User');
			return;			
		}
		
		if (li_PrimaryEnformUsersUpd.size() > 0)
		{
			SP_Utilities.EnformUpdatedByAccountTrigger = true;
			update li_PrimaryEnformUsersUpd;
		}
					

		// Call the web service from a future method
		if (system.isBatch() || system.isFuture() || system.isScheduled())
			SP_EnformUtilities.notifyIntelematicsNoFuture(set_ChangedEnformUserIds, 'update', true);
		else
			SP_EnformUtilities.notifyIntelematics(set_ChangedEnformUserIds, 'update', true);
	}


/*
	Where an Account has been deleted / merged, check for an Enform User and if found notify Intelematics
*/
	public static void doSyncNotification_AccounttoEnformUser(map<id, Account> oldMap)
	{
		list<ID> li_EnformUserIds = new list<ID>();
		list<ID> li_EnformUserAccountIds = new list<ID>();
		list<ID> li_OwnerIds = new list<ID>();
		
		// First look for changed fields on an Owner record
		for (Account sOldAccount : oldMap.values())
		{
			if (sOldAccount.RecordTypeId != SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
			{
				system.debug('######## Account ' + sOldAccount.Id + ' is not a Person Account Owner.');
				continue;
			}

			set_ChangedAccountIds.add(sOldAccount.Id);
		}
		
		if (set_ChangedAccountIds.isEmpty())
		{
			system.debug('######## There is no Accounts deleted by a non-Intelematics User');
			return;
		}
		
				
		// Now look for Enform Users
		for (Enform_User__c [] arrEnformUsers :	[	select	id, Owner_ID__c, Owner_ID__r.OwnerId
													
													
													from	Enform_User__c
													where	Owner_ID__c in :set_ChangedAccountIds and
													IsPrimaryUser__c = true
												])
		{
			for (Enform_User__c sEnformUser : arrEnformUsers)
			{
				//set_ChangedEnformUserIds.add(sEnformUser.Id);
				li_EnformUserIds.add(sEnformUser.Id);
				li_EnformUserAccountIds.add(sEnformUser.Owner_ID__c);
				li_OwnerIds.add(sEnformUser.Owner_ID__r.OwnerId);
			}
		}
		
		if (li_EnformUserIds.isEmpty())
		{
			system.debug('######## There is no Enform Users associated with Accounts deleted by the non-Intelematics User');
			return;			
		}		

		// Call the web service from a future method
		if (system.isBatch() || system.isFuture() || system.isScheduled())
			SP_EnformUtilities.notifyIntelematicsNoFuture(li_EnformUserIds, li_EnformUserAccountIds, li_OwnerIds, null, true);
		else
			SP_EnformUtilities.notifyIntelematics(li_EnformUserIds, li_EnformUserAccountIds, li_OwnerIds, null, true);
	}
	
	////////// TEST METHODS ///////////////////
	
	/*@isTest(SeeAllData=true)
	public static void testMethodTriggerNotifyIntelematicsOnChange()
	{
		Test.startTest();
		
		List<Account> testAccounts = new List<Account>();
		
		Account sAccountWithEU = new Account();
		sAccountWithEU.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccountWithEU.LastName				= 'TestLast1';
		sAccountWithEU.PersonEmail			= 'test1@email.com';
		
		testAccounts.add(sAccountWithEU);
		
		Account sAccountWithOutEU = new Account();
		sAccountWithOutEU.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccountWithOutEU.LastName				= 'TestLast2';
		sAccountWithOutEU.PersonEmail			= 'test2@email.com';
		
		testAccounts.add(sAccountWithOutEU);	
		
		insert testAccounts;
		
		Enform_User__c testEU = new Enform_User__c(Owner_Id__c = sAccountWithEU.Id,
												   Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last',
												   IsPrimaryUser__c = true);
		insert testEU;
		
		sAccountWithEU.LastName = 'Test3';
		sAccountWithOutEU.LastName = 'Test4';
		
		update testAccounts;
		
		delete testEU;
		delete testAccounts;
		
		Test.stopTest();
	}*/
}