public class SPException extends Exception
{
	public enum Severity
	{
		CRITICAL,
		MAJOR,
		MODERATE,
		MINOR,
		COSMETIC
	}
	
	public class ClassInfo 
	{
		public string 	ClassName 				{get; set;}
		public string 	MethodName 				{get; set;}
		public integer 	LineNumber 				{get; set;}		
	}
	
	public class Info
	{
		public  string			CompanyName		{get; set;}
		public List<ClassInfo>	ClassInfoList	{get; set;}
		public string 			Type			{get; set;}
		public string			Message			{get; set;}
		public Severity 		SeverityLevel 	{get; set;}
		
		public Info()
		{
			ClassInfoList = new List<ClassInfo>();
			CompanyName = [select Name from Organization limit 1][0].Name;
		}
	}
	
	private SPException.Severity m_Severity;
	
	
	public void setSeverity(SPException.Severity severityLevel)
	{
		m_Severity = severityLevel;
	}
	
	public SPException.Severity getSeverity()
	{
		return m_Severity;
	}
	
	public void setErrorData(string message, SPException.Severity severityLevel)
	{
		setMessage(message);
		setSeverity(severityLevel);
	}
	
	public void Log()
	{
		Log(null, null, null);
	}
	
	public void Log(ID caseOwner, ID accountID)
	{
		Log(caseOwner, accountID, null);
	}
	
	public void Log(ID caseOwner, ID accountID, string supportEmail)
	{
		Info information = getExceptionInfo(this);
		information.SeverityLevel = m_Severity;
		
		processException(information, supportEmail);
		
		if (caseOwner != null)
			CreateExceptionCase(information, caseOwner, accountID);
	}
	
	private static void CreateExceptionCase(Info information, ID caseOwner, ID accountId)
	{
		List<Account> accounts = [select Id from Account where Id = :accountId];
		Case sCase = new Case();

		sCase.RecordTypeId		= SPCacheLexusEnformMetadata.getDefaultCaseRecordType();		// Enform
		sCase.Type				= SPCacheLexusEnformMetadata.getDefaultCaseType();				//
		sCase.OwnerId			= caseOwner;													// Owner Account
		sCase.Status			= SPCacheLexusEnformMetadata.getDefaultCaseStatus();			// In Progress
		sCase.Origin			= SPCacheLexusEnformMetadata.getDefaultCaseOrigin();			// 
		sCase.Subject			= 'Enform Data Synchronisation - ' + DateTime.now();
		sCase.Description		= 'EXCEPTION INFORMATION: ' + information;
		sCase.AccountId			= (accounts.size() > 0) ? accountId : null;
        
        System.debug('CreateExceptionCase description: ' + sCase.Description);
		insert sCase;

	}
	
	public static void LogSystemException(Exception ex, ID caseOwner, ID accountID)
	{
		LogSystemException(ex, caseOwner, accountID, null);
	}
	
	public static void LogSystemException(Exception ex)
	{
		LogSystemException(ex, null, null, null);
	}
	
	public static void LogSystemException(Exception ex, ID caseOwner, ID accountID, string supportEmail)
	{
		Info information = getExceptionInfo(ex);
		information.SeverityLevel = SPException.Severity.CRITICAL;
		
		processException(information, supportEmail);
		
		if (caseOwner != null)
			CreateExceptionCase(information, caseOwner, accountID);
	}
	
	private static Info getExceptionInfo(Exception ex)
	{
		Info information = new Info();
		
		try
		{
			information.Type = ex.getTypeName();
			information.Message = ex.getMessage();
			
			string strStackTrace = ex.getStackTraceString();
			
			system.debug('%%%%%%%%%%%%%%%%%%%%%%% STACK ' + strStackTrace);
			List<string> subStrings = strStackTrace.split('\n');
			
			for (integer i = 0; i < subStrings.size(); i++)
			{
				integer indexOfFirstDot = subStrings[i].indexOf('.');
				if (indexOfFirstDot == -1)
					continue;
					
				integer indexOfSecondDot = subStrings[i].indexOf('.', indexOfFirstDot + 1);
				integer indexOfLastColumn; 
				
				if (indexOfSecondDot != -1)
					indexOfLastColumn = subStrings[i].indexOf(':', indexOfSecondDot);
				else
					indexOfLastColumn = subStrings[i].indexOf(':', indexOfFirstDot);
							
				ClassInfo classInformation = new ClassInfo();
				
				if (indexOfSecondDot != -1)
				{
					classInformation.ClassName = subStrings[i].substring(indexOfFirstDot + 1, indexOfSecondDot);
					classInformation.MethodName = subStrings[i].substring(indexOfSecondDot + 1, indexOfLastColumn);
				}
				else
					classInformation.ClassName = subStrings[i].substring(indexOfFirstDot + 1, indexOfLastColumn);
				
				List<String> lineParts = subStrings[i].split(',');
				integer indexOfLineNum = lineParts[0].indexOf('line ');
				classInformation.LineNumber = Integer.valueOf(lineParts[0].substring(indexOfLineNum + 5, lineParts[0].length()));		
	
				information.ClassInfoList.add(classInformation);
			}
			
			return information;
		}
		catch(Exception e)
		{
			return information;
		}
	}

	private static void processException(Info information, string supportEmail)
	{
		string debugMessage = '###### EXCEPTION INFO\n' +
							  '###### COMPANY: ' + information.CompanyName + '\n###### CLASS INFO LIST:\n';
		string strEmailMessage 	= 'Hi,<br/><br/>';
		strEmailMessage			+= 'There is an exception from ' + information.CompanyName + '. Details are given below.<br/><br/>';
		strEmailMessage			+= 'TYPE: ' + information.Type + '<br/>';
		strEmailMessage			+= 'MESSAGE: ' + information.Message + '<br/>';
		strEmailMessage			+= 'SEVERITY: ' + information.SeverityLevel + '<br/>';
		strEmailMessage			+= 'STACK TRACE: <br/><br/>';
			
		for (integer i = 0; i < information.ClassInfoList.size(); i++)
		{
			strEmailMessage		+= '&nbsp;&nbsp;&nbsp;&nbsp;CLASS: ' + information.ClassInfoList[i].ClassName + '<br/>';
			strEmailMessage		+= '&nbsp;&nbsp;&nbsp;&nbsp;METHOD: ' + information.ClassInfoList[i].MethodName + '<br/>';
			strEmailMessage		+= '&nbsp;&nbsp;&nbsp;&nbsp;LINE: ' + information.ClassInfoList[i].LineNumber + '<br/><br/>';
			
			debugMessage += ('######### CLASS: ' + information.ClassInfoList[i].ClassName + '\n######### METHOD: ' +
					information.ClassInfoList[i].MethodName + '\n######### LINE: ' + information.ClassInfoList[i].LineNumber + '\n\n');
		}
		
		strEmailMessage 		+= 'Administrator';
		
		debugMessage += ('###### TYPE: ' + 
					 	information.Type + '\n###### MESSAGE: ' + information.Message + '\n###### SEVERITY LEVEL: ' + 
					 	information.SeverityLevel);
		
		system.debug(debugMessage);
		
		if (supportEmail != null)
		{
			// Call Email Routine here to send the exception details to SqwarePeg support team
			
			List<Messaging.SingleEmailMessage> allMessages = new List<Messaging.SingleEmailMessage>();
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	 
	        mail.setToAddresses(new string[] { supportEmail });
	   		mail.setSenderDisplayName(information.CompanyName);
	   		mail.setSaveAsActivity(false);  
	   		mail.setHtmlBody(strEmailMessage);
	   		mail.setSubject('IMPORTANT: ' + information.CompanyName + ' reports a ' + information.SeverityLevel.name() + 
	   						' Exception');
	   		
	   		allMessages.add(mail);
	   		
	   		Messaging.sendEmail(allMessages);		   
		}
	} 	
}