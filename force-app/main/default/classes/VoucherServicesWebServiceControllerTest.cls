// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData = true)
public class VoucherServicesWebServiceControllerTest {
	// DEVELOPER NOTES: These are just all coverage test methods; The assertion test methods are in the corresponding
	// test method classes for each of the handler classes
	public static Account partnerbranch;
    public static VO_Offer_Detail__c vood;
    
    static testmethod void attendantLogin(){
        dataSetup();
        VoucherServicesWebServiceController cont = new VoucherServicesWebServiceController();
        cont.username = partnerbranch.Partner_Branch_Username__c;
        cont.password = partnerbranch.Partner_Branch_Password__c;
        cont.attendantLogin();
    }
    
    static testmethod void valetPasswordResend(){
        dataSetup();
        VoucherServicesWebServiceController cont = new VoucherServicesWebServiceController();
        cont.partnerBranchIdentifier = partnerbranch.Partner_Branch_Username__c;
        cont.valetPasswordResend();
    }
    
    static testmethod void redeemVoucher(){
        dataSetup();
        VoucherServicesWebServiceController cont = new VoucherServicesWebServiceController();
        cont.partnerBranchId = partnerbranch.Id;
        cont.redeemVoucher.Voucher_Type__c = 'Valet Service';
        cont.vOOfferDetailID = vood.Id;
        cont.compFlag = true;
        cont.compComment = 'Test';
        cont.inputErrCode = 'Test';
        cont.redeemVoucher();
    }
    
    static testmethod void getVoucherAvailability(){
        dataSetup();
        VoucherServicesWebServiceController cont = new VoucherServicesWebServiceController();
        cont.vcPartnerBranchId = partnerbranch.Id;
        cont.regoNumber = 'test';
        cont.vcVoucher.Voucher_Type__c = 'Valet Service';
        cont.getVoucherAvailability();
    }
    
    private static void dataSetup(){
        Id PARTNERBRANCHRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Branch').getRecordTypeId();
        partnerbranch = new Account(Name = 'Partner Branch', RecordTypeId = PARTNERBRANCHRECORDTYPEID,
                                   Partner_Branch_Username__c = 'pusername', Partner_Branch_Password__c = 'password', BillingCity = 'Test', BillingState = 'Test');
        insert partnerbranch;
        vood = new VO_Offer_Detail__c();
    }
}