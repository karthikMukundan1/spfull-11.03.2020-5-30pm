public without sharing class BatchUpdateAccountEmailTemp implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
    public BatchUpdateAccountEmailTemp(){
        
    }
    
    public void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){ 
        return Database.getQueryLocator([select Id, PersonEmail from Account where IsPersonAccount = true 
                                         and PersonEmail != null
                                         and PersonEmail != 'test@mailinator.com']);
        
    }
    
    public void execute(Database.BatchableContext BC, List<Account> scope) {
    	for (Account a : scope){
            a.PersonEmail = 'test@mailinator.com';
        }
        
        update scope;
    }
    
    public void finish(Database.BatchableContext BC) {
        
    }
}