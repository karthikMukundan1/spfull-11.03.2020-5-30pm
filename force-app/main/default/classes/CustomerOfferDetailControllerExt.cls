/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: COD controller extension to allow LCAC to recalculate COD counts and ates 
 * 					from the COD record page via the CalculateCountsCOD VF page
 * VF PAGE USING THIS CLASS: CalculateCountsCOD
 * TEST CLASS: CustomerOfferDetailControllerExtTest
 * */
public with sharing class CustomerOfferDetailControllerExt {
    Customer_Offer_Detail__c cod {get;set;}
    public CustomerOfferDetailControllerExt(ApexPages.StandardController stdCon){
        this.cod = (Customer_Offer_Detail__c)stdCon.getRecord();
        
    }
    public PageReference runBatch(){
        BatchCalculateCustomerOfferDetails codBatch = new BatchCalculateCustomerOfferDetails(cod.Id);
        Database.executeBatch(codBatch);
        return new PageReference('/' + cod.Id);
    }
}