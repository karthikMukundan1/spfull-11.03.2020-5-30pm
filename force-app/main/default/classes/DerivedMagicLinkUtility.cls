public without sharing class DerivedMagicLinkUtility {
    // utility for getting derived magic link
    //VD 25/2/2020 - Added Code to encrypt parameters in the link.
    
	@future(callout=true)
    public static void getDerivedMagicLinkUrls(Set<Id> voIdSet){
        List<Vehicle_Ownership__c> voList = [select Id, Customer__c, Magic_Link_URL__c , Hashed_ID__c from Vehicle_Ownership__c where Id = :voIdSet];
        List<Vehicle_Ownership__c> voUpdateList = new List<Vehicle_Ownership__c>();
        for (Vehicle_Ownership__c vo : voList){
           voUpdateList.add(getDerivedMagicLinkUrl(vo.Customer__c, vo.Id, vo.Magic_Link_URL__c, vo.Hashed_ID__c));
        }
        if (voUpdateList.size() > 0){
            update voUpdateList;
        }
    }
    
    public static Vehicle_Ownership__c getDerivedMagicLinkUrl(Id sfAcctId, Id sfVoId, String sfMagicLink, String HashedId){
        Vehicle_Ownership__c vo = new Vehicle_Ownership__c(Id = sfVoId);
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type','application/json'); 
        req.setEndpoint(bIoSettings.EndpointURL__c);
        req.setMethod('POST');
        
        //Get Hashed id If null - VD Added
        Blob targetBlob = Blob.valueOf(sfVoId);
        Blob hash = Crypto.generateDigest('SHA-256', targetBlob);
        String encryptedString = EncodingUtil.convertToHex(hash);
             
        //Modifiy Magiclink URL with Hashed Id
         PageReference encryptedurl = new PageReference(sfMagicLink);
         Map<String,String> parameters = encryptedurl.getParameters();
        //remove parameters
        Set<String> keysToRemove = new Set<String>{'sfid', 'void'};
        encryptedurl.getParameters().keySet().removeAll(keysToRemove);
        
        //Add New Parameters
         encryptedurl.getParameters().put('token', encryptedString);
         String stringURL=encryptedurl.getUrl();
        //String stringURL= String.valueOf(encryptedurl);
        System.debug(stringURL);
         //Send new encrypted link.
         req.setBody(generateBodyMap(sfAcctId, sfVOId, stringURL, encryptedString));
      
 //       req.setBody(generateBodyMap(sfAcctId, sfVOId, sfMagicLink));
 // Send the message
        Http http = new Http();
        HTTPResponse res = http.send(req);
        system.debug('## response body: ' + res.getBody());
        JSONParser parser = JSON.createParser(res.getBody());
        String derivedMagicLinkURL = null;
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'url')) {
                parser.nextToken();
                derivedMagicLinkURL = parser.getText();
            }
        }
        system.debug('## derivedMagicLinkURL: ' + derivedMagicLinkURL);
       
       //VD Update Hashed ID
        if (derivedMagicLinkURL != null){
            vo.DerivedMagicLinkURL__c = derivedMagicLinkURL;
            vo.Hashed_ID__c = encryptedString;
            }
        
        return vo;
        
    }
    
    private static BranchIOSettings__c bIoSettings {
        get {
            if (bIoSettings == null){
                bIoSettings = BranchIOSettings__c.getInstance();
            }
            return bIoSettings;
        }
        set;
    }
    
    private static String generateBodyMap(Id sfAcctId, Id sfVOId, String sfMagicLink, String sfToken){
        Map<String, Object> nameValueMap = new Map<String, Object>();
        nameValueMap.put('branch_key', bIoSettings.BranchKey__c);
        nameValueMap.put('campaign', 'Registration');
        nameValueMap.put('channel', 'Email');
        nameValueMap.put('tags', new List<String>());
        Map<String, String> dataMap = new Map<String, String>();
        dataMap.put('$sfid', sfAcctId);
        dataMap.put('$void', sfVOId);
        dataMap.put('$token', sfToken);
        dataMap.put('$desktop_url', sfMagicLink);
        nameValueMap.put('data', dataMap);
        return JSON.Serialize(nameValueMap);
    }
}