@isTest
public class SP_VehicleOwnershipManagementTest 
{
	private static Account CreateCurrentOwner()
	{
		Account CurrentOwner = new Account();
		CurrentOwner.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		CurrentOwner.FirstName				= 'TestFirst1';
		CurrentOwner.LastName				= 'TestLast1';
		CurrentOwner.PersonEmail			= 'test1@email.com';
		CurrentOwner.PersonMobilePhone		= '1234567';
		
		insert CurrentOwner;
		
		return CurrentOwner;
	}
	
	private static Account CreateNewOwner()
	{
		Account CurrentOwner = new Account();
		CurrentOwner.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		CurrentOwner.FirstName				= 'TestNewFirst';
		CurrentOwner.LastName				= 'TestNewLast';
		CurrentOwner.PersonEmail			= 'testnew@email.com';
		CurrentOwner.PersonMobilePhone		= '12345678';
		
		insert CurrentOwner;
		
		return CurrentOwner;
	}
	
	private static Asset__c CreateTestAsset(Date origSalesDate)
	{
		Asset__c testAsset = new Asset__c(Name = 'test1', Original_Sales_Date__c = origSalesDate);
		insert testAsset;	
		
		return testAsset;
	}
	
	private static Vehicle_Ownership__c CreateVOForOwner(boolean IsEnformed, Date origSalesDate, ID owner)
	{
		Asset__c testAsset = CreateTestAsset(origSalesDate);
		
		Vehicle_Ownership__c testVO = new Vehicle_Ownership__c(Enform_Registered__c = IsEnformed,
																Customer__c = owner,
																Status__c = 'Active',
																AssetID__c = testAsset.Id);
		insert 	testVO;
		
		return testVO;
	}
	
	private static Enform_User__c CreateEnformUser(ID ownerID)
	{
    	Enform_User__c EnfUser = new Enform_User__c(Owner_Id__c = ownerID,
					   Mobile_Phone__c = '1234567',
					   Email__c = 'test1@email.com',
					   First_Name__c = 'TestFirst1',
					   Last_Name__c = 'TestLast1',
					   IsPrimaryUser__c = true);
					   
		insert EnfUser;
		
		return EnfUser;
	}
	
	// Vehicle Ownership Inserted - Enform Registered
	// No existing active ownership 
	// Vehicle owner should not have an enform user
	
	// Result
	
	// New vehicle ownership inserted
	// New primary enform user inserted
	// Enform Expiration date of owner is set as original sales date of new vehicle + 4 years
	@isTest(SeeAllData=true)
    public static void TestCase001() 
    {
    	Test.startTest();
    	
    	Account owner = CreateCurrentOwner();
    	
    	Vehicle_Ownership__c NonEnfVO = CreateVOForOwner(false,  Date.today().addDays(-5), owner.Id);
    	
    	Date enfVOOrigSalesDate =  Date.today().addDays(-6);
    	Vehicle_Ownership__c EnfVO = CreateVOForOwner(true, enfVOOrigSalesDate, owner.Id);
    	
    	Test.stopTest();
    	
    	system.assertNotEquals(EnfVO.Id, null);
    	
    	List<Enform_User__c> enformUsers = [select Id from Enform_User__c where Owner_ID__c = :EnfVO.Customer__c];
    	system.assertEquals(enformUsers.size(), 1);
 
 		Account ownerAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :EnfVO.Customer__c]; 
 		system.assertEquals(ownerAfter.Enform_Expiration_Date_Vehicle_License__c, enfVOOrigSalesDate.addYears(4));   	
    }
    
 	// Vehicle Ownership Inserted - Enform Registered
	// Existing active ownership for another vehicle
	// Vehicle owner should have an enform user
	// New vehicle should have a latest original sales date
	
	// Result
	
	// New vehicle ownership inserted
	// No primary enform user inserted
	// Enform Expiration date of owner is set as original sales date of new vehicle + 4 years
	@isTest(SeeAllData=true)
    public static void TestCase002() 
    {
    	Test.startTest();
    	    	
    	Account owner = CreateCurrentOwner();
    	
    	Date enfVOOrigSalesDate =  Date.today().addDays(-6);
    	Vehicle_Ownership__c ExisEnfVO = CreateVOForOwner(true, enfVOOrigSalesDate, owner.Id);
    	
    	Enform_User__c EnfUser = [select Id from Enform_User__c where Owner_ID__c = :owner.Id];
    	
    	Date licenseDateBefore = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
    	
    	Date latestOrigSalesDate = enfVOOrigSalesDate.addDays(1);
    	
    	Vehicle_Ownership__c EnfVO = CreateVOForOwner(true, latestOrigSalesDate, owner.Id);
     	
    	Test.stopTest();
    	
    	system.assertNotEquals(EnfVO.Id, null);
    	
    	List<Enform_User__c> enformUsers = [select Id from Enform_User__c where Owner_ID__c = :EnfVO.Customer__c];
    	system.assertEquals(enformUsers.size(), 1);
    	system.assertEquals(enformUsers[0].Id, EnfUser.Id);
 
 		Date licenseDateAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :EnfVO.Customer__c].Enform_Expiration_Date_Vehicle_License__c;
 		
 		system.assertNotEquals(licenseDateBefore, licenseDateAfter);  
 		system.assertEquals(licenseDateAfter, latestOrigSalesDate.addYears(4));      
    } 
    
 	// Vehicle Ownership Inserted - Enform Registered
	// Existing active ownership for another vehicle
	// Vehicle owner should have an enform user
	// Old vehicle should have a latest original sales date
	
	// Result
	
	// New vehicle ownership inserted
	// No primary enform user inserted
	// No change for Enform Expiration date of owner 
	@isTest(SeeAllData=true)
    public static void TestCase003() 
    {
    	Test.startTest();
    	    	
    	Account owner = CreateCurrentOwner();
    	
    	Date enfVOOrigSalesDate =  Date.today().addDays(-6);
    	Vehicle_Ownership__c ExisEnfVO = CreateVOForOwner(true, enfVOOrigSalesDate, owner.Id);
    	
    	Enform_User__c EnfUser = [select Id from Enform_User__c where Owner_ID__c = :owner.Id];
    	
    	Date licenseDateBefore = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
    	
    	Date latestOrigSalesDate = enfVOOrigSalesDate.addDays(-1);
    	
    	Vehicle_Ownership__c EnfVO = CreateVOForOwner(true, latestOrigSalesDate, owner.Id);
     	
    	Test.stopTest();
    	
    	system.assertNotEquals(EnfVO.Id, null);
    	
    	List<Enform_User__c> enformUsers = [select Id from Enform_User__c where Owner_ID__c = :EnfVO.Customer__c];
    	system.assertEquals(enformUsers.size(), 1);
    	system.assertEquals(enformUsers[0].Id, EnfUser.Id);
 
 		Date licenseDateAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :EnfVO.Customer__c].Enform_Expiration_Date_Vehicle_License__c;
 		
 		system.assertEquals(licenseDateBefore, licenseDateAfter);  
    }       

	// Vehicle Ownership Inserted - Enform Not Registered
	// No existing active ownership 
	// Vehicle owner should not have an enform user
	
	// Result
	
	// New vehicle ownership inserted
	// No primary enform user inserted
	// No change for Enform Expiration date of owner
	@isTest(SeeAllData=true)
    public static void TestCase004() 
    {
    	Test.startTest();
    	
    	Account owner = CreateCurrentOwner();
    	
    	Vehicle_Ownership__c NonEnfVO = CreateVOForOwner(false,  Date.today().addDays(-5), owner.Id);
    	
    	Date enfVOOrigSalesDate =  Date.today().addDays(-6);
    	Vehicle_Ownership__c NonEnfVOOther = CreateVOForOwner(false, enfVOOrigSalesDate, owner.Id);
    	
    	Test.stopTest();
    	
    	system.assertNotEquals(NonEnfVOOther.Id, null);
    	
    	List<Enform_User__c> enformUsers = [select Id from Enform_User__c where Owner_ID__c = :NonEnfVOOther.Customer__c];
    	system.assertEquals(enformUsers.size(), 0);
 
 		Account ownerAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :NonEnfVOOther.Customer__c]; 
 		system.assertEquals(ownerAfter.Enform_Expiration_Date_Vehicle_License__c, null);       
    } 
    
    // Vehicle Ownership Updated - Enform Not Registered -> Enform Registered
	// Existing active ownership with Enform Not Registered
	// Vehicle owner should not have an enform user
	
	// Result
	
	// Vehicle ownership updated
	// New primary enform user inserted
	// Enform Expiration date of owner is set as original sales date of the vehicle + 4 years
	@isTest(SeeAllData=true)
    public static void TestCase005() 
    {
    	Test.startTest();
    	
    	Account owner = CreateCurrentOwner();
    	
    	Date enfVOOrigSalesDate = Date.today().addDays(-5);
    	
    	Vehicle_Ownership__c NonEnfVO = CreateVOForOwner(false,  enfVOOrigSalesDate, owner.Id);
    	
    	Date licenseDateBefore = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :NonEnfVO.Customer__c].Enform_Expiration_Date_Vehicle_License__c; 
    	
		NonEnfVO.Enform_Registered__c = true;
		update NonEnfVO;
    	
    	Test.stopTest();
    	
    	NonEnfVO = [select Enform_Registered__c, Customer__c from Vehicle_Ownership__c where Id = :NonEnfVO.Id];
      	system.assertEquals(NonEnfVO.Enform_Registered__c, true);
    	
    	List<Enform_User__c> enformUsers = [select Id from Enform_User__c where Owner_ID__c = :NonEnfVO.Customer__c];
    	system.assertEquals(enformUsers.size(), 1);
 
 		system.assertEquals(licenseDateBefore, null);      
 		Date licenseDateAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :NonEnfVO.Customer__c].Enform_Expiration_Date_Vehicle_License__c; 
 		system.assertEquals(licenseDateAfter, enfVOOrigSalesDate.addYears(4));      
    }
    
  	// Vehicle Ownership Updated - Enform Not Registered -> Enform Registered
	// Existing active ownership with Enform Not Registered
	// Vehicle owner should have an enform user
	
	// Result
	
	// Vehicle ownership updated
	// No primary enform user inserted
	// Enform Expiration date of owner is set as original sales date of new vehicle + 4 years
	@isTest(SeeAllData=true)
    public static void TestCase006() 
    {
    	Test.startTest();
    	    	
    	Account owner = CreateCurrentOwner();
    	
    	Date enfVOOrigSalesDate =  Date.today().addDays(-6);
    	Vehicle_Ownership__c ExisEnfVO = CreateVOForOwner(true, enfVOOrigSalesDate, owner.Id);
    	
    	Enform_User__c EnfUser = [select Id from Enform_User__c where Owner_ID__c = :owner.Id];
    	
    	Date licenseDateBefore = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
    	
    	Date latestOrigSalesDate = enfVOOrigSalesDate.addDays(1);
    	
    	Vehicle_Ownership__c EnfVO = CreateVOForOwner(false, latestOrigSalesDate, owner.Id);
    	
    	EnfVO.Enform_Registered__c = true;
    	update EnfVO;
     	
    	Test.stopTest();
    	
    	EnfVO = [select Enform_Registered__c, Customer__c from Vehicle_Ownership__c where Id = :EnfVO.Id];
      	system.assertEquals(EnfVO.Enform_Registered__c, true);
    	
    	List<Enform_User__c> enformUsers = [select Id from Enform_User__c where Owner_ID__c = :EnfVO.Customer__c];
    	system.assertEquals(enformUsers.size(), 1);
    	system.assertEquals(enformUsers[0].Id, EnfUser.Id);
 
 		Date licenseDateAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
 		
 		system.assertNotEquals(licenseDateBefore, licenseDateAfter);  
 		system.assertEquals(licenseDateAfter, latestOrigSalesDate.addYears(4));       
    }  
    
    // Vehicle Ownership Updated - Enform Registered -> Enform Not Registered
	// Existing active ownership with Enform Not Registered
	// Vehicle owner should have an enform user
	
	// Result
	
	// Vehicle ownership updated
	// No primary enform user inserted
	// Enform Expiration date of owner is set as original sales date of the vehicle + 4 years
	@isTest(SeeAllData=true)
    public static void TestCase007() 
    {
    	Test.startTest();
    	    	
    	Account owner = CreateCurrentOwner();
    	
    	Date enfVOOrigSalesDate =  Date.today().addDays(-6);
    	Vehicle_Ownership__c OldEnfVO = CreateVOForOwner(true, enfVOOrigSalesDate, owner.Id);
    	
    	Enform_User__c EnfUser = [select Id from Enform_User__c where Owner_ID__c = :owner.Id];
   	
    	Date latestOrigSalesDate = enfVOOrigSalesDate.addDays(1);
    	
    	Vehicle_Ownership__c NewEnfVO = CreateVOForOwner(true, latestOrigSalesDate, owner.Id);
    	
    	Date licenseDateBefore = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
    	
    	NewEnfVO.Enform_Registered__c = false;
    	update NewEnfVO;
     	
    	Test.stopTest();
    	
    	NewEnfVO = [select Enform_Registered__c, Customer__c from Vehicle_Ownership__c where Id = :NewEnfVO.Id];
      	system.assertEquals(NewEnfVO.Enform_Registered__c, false);
    	
    	List<Enform_User__c> enformUsers = [select Id from Enform_User__c where Owner_ID__c = :NewEnfVO.Customer__c];
    	system.assertEquals(enformUsers.size(), 1);
    	system.assertEquals(enformUsers[0].Id, EnfUser.Id);
 
 		Date licenseDateAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
 		
 		system.assertNotEquals(licenseDateBefore, licenseDateAfter);  
 		system.assertEquals(licenseDateAfter, enfVOOrigSalesDate.addYears(4));      
    }  
    
    // Vehicle Ownership Deleted
	// Two Enform registered Vehicle Ownerships
	// Vehicle owner should have an enform user
	
	// Result
	
	// Latest Vehicle Ownership is deleted
	// Enform Expiration date of owner is set as original sales date of the other vehicle + 4 years
	@isTest(SeeAllData=true)
    public static void TestCase008() 
    {
        Test.startTest();
    	    	
    	Account owner = CreateCurrentOwner();
    	
    	Date enfVOOrigSalesDate =  Date.today().addDays(-6);
    	Vehicle_Ownership__c ExisEnfVO = CreateVOForOwner(true, enfVOOrigSalesDate, owner.Id);
    	
    	Enform_User__c EnfUser = [select Id from Enform_User__c where Owner_ID__c = :owner.Id];
    	
    	Date licenseDateBefore = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
    	
    	Date latestOrigSalesDate = enfVOOrigSalesDate.addDays(1);
    	
    	Vehicle_Ownership__c EnfVO = CreateVOForOwner(true, latestOrigSalesDate, owner.Id);
    	
    	Date licenseDateAfter = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :EnfVO.Customer__c].Enform_Expiration_Date_Vehicle_License__c;
    	
    	delete EnfVO;
     	
    	Test.stopTest();
    	
		List<Vehicle_Ownership__c> VOs = [select Id from Vehicle_Ownership__c where Customer__c = :owner.Id];
		system.assertEquals(VOs.size(), 1);
		system.assertEquals(VOs[0].Id, ExisEnfVO.Id);
		
 		Date licenseDateAfterDeletion = [select Enform_Expiration_Date_Vehicle_License__c from Account where Id = :owner.Id].Enform_Expiration_Date_Vehicle_License__c;
 		
 		system.assertNotEquals(licenseDateAfter, licenseDateAfterDeletion);  
 		system.assertEquals(licenseDateAfterDeletion, enfVOOrigSalesDate.addYears(4));   
    }     
}