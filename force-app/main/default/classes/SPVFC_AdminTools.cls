public with sharing class SPVFC_AdminTools
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	public boolean	mp_bAccountBatchFlagClear		{get; set;}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections


	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SPVFC_AdminTools()
	{
		
	}


	public pageReference Init()
	{
		// Check and see if any records have the Batch Processed flag set
		list<Account>	li_Accounts		= new list<Account>();
		list<Case>		li_Cases		= new list<Case>();
		list<Asset__c>	li_Assets		= new list<Asset__c>();

		mp_bAccountBatchFlagClear = true;

		try
		{
			li_Accounts =	[
								select	Batch_Processed__c
								from	Account
								where	Batch_Processed__c = true
								limit	10
							];
		}
		catch (System.LimitException e)
		{
			
		}

		if (!li_Accounts.isEmpty())
		{
			mp_bAccountBatchFlagClear = false;
		}

		return null;
	}


	/***********************************************************************************************************
		Button Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public pageReference startHistoricalServiceDataBatch()
	{
		SP_HistoricalServiceDataBatch objSP_HistoricalServiceDataBatch = new SP_HistoricalServiceDataBatch();
        ID batchprocessid = Database.executeBatch(objSP_HistoricalServiceDataBatch);

		return null;
	}


	public Pagereference startDealerIntegrityBatch()
	{
		SP_DealerIntegrityBatch objSP_DealerIntegrityBatch = new SP_DealerIntegrityBatch();
        ID batchprocessid = Database.executeBatch(objSP_DealerIntegrityBatch);

		return null;
	}
	
	public Pagereference startDealerIntegrity3Batch()
	{
		SP_DealerIntegrity3Batch objSP_DealerIntegrity3Batch = new SP_DealerIntegrity3Batch();
        ID batchprocessid = Database.executeBatch(objSP_DealerIntegrity3Batch);

		return null;
	}


	public Pagereference startDealerIntegrity2Batch()
	{
		SP_DealerIntegrity2Batch objSP_DealerIntegrity2Batch = new SP_DealerIntegrity2Batch();
        ID batchprocessid = Database.executeBatch(objSP_DealerIntegrity2Batch);

		return null;
	}


	public Pagereference startModelsOwnedBatch()
	{
		SP_OwnerModelsBatch objSP_OwnerModelsBatch = new SP_OwnerModelsBatch();
        ID batchprocessid = Database.executeBatch(objSP_OwnerModelsBatch);

		return null;
	}


	public Pagereference startClearAccountFlagBatch()
	{
		SP_ClearAccountFlagBatch objSP_ClearAccountFlagBatch = new SP_ClearAccountFlagBatch();
        ID batchprocessid = Database.executeBatch(objSP_ClearAccountFlagBatch);

		return null;
	}


	public pageReference gotoTopTen()
	{
		pageReference pageRedirect;

		pageRedirect = new PageReference('/apex/SPVFP_TopTen');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	public Pagereference startServicedYearBatch()
	{
		SP_ServicedYearBatch objSP_ServicedYearBatch = new SP_ServicedYearBatch();
        ID batchprocessid = Database.executeBatch(objSP_ServicedYearBatch);

		return null;
	}


	public pageReference startServiceIntegrationBatch()
	{
		SP_ServiceIntegrationBatch objSP_ServiceIntegrationBatch = new SP_ServiceIntegrationBatch();
        ID batchprocessid = Database.executeBatch(objSP_ServiceIntegrationBatch, 10);

		return null;
	}


	public pageReference gotoDataIntegrityChecker()
	{
		pageReference pageRedirect;

		pageRedirect = new PageReference('/apex/SPVFP_DataIntegrityChecker');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	public pageReference gotoDeDupTool()
	{
		pageReference pageRedirect;

		pageRedirect = new PageReference('/apex/SPVFP_FileDeDupTool');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	public Pagereference startDeleteAllAFVBatch()
	{
		SP_DeleteAllAFVBatch objSP_DeleteAllAFVBatch = new SP_DeleteAllAFVBatch();
        ID batchprocessid = Database.executeBatch(objSP_DeleteAllAFVBatch);

		return null;
	}

/*
	public Pagereference startAccountAddressCleanBatch()
	{
		SP_AccountAddressCleanBatch objSP_AccountAddressCleanBatch = new SP_AccountAddressCleanBatch();
        ID batchprocessid = Database.executeBatch(objSP_AccountAddressCleanBatch);

		return null;
	}
*/

	public Pagereference startTopTenBatch()
	{
//		SP_TopTenBatch objSP_TopTenBatch = new SP_TopTenBatch();
//      ID batchprocessid = Database.executeBatch(objSP_TopTenBatch);

		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public pageReference actionCancel()
	{
		pageReference pageRedirect;

		pageRedirect = new PageReference('/o');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSPVFC_AdminTools()
	{
		SPVFC_AdminTools tools = new SPVFC_AdminTools();

		tools.Init();

		test.startTest();

		// Limit of five batch calls!
//		tools.startHistoricalServiceDataBatch();

		tools.startDealerIntegrityBatch();

		tools.startDealerIntegrity2Batch();

		tools.startModelsOwnedBatch();

		tools.startClearAccountFlagBatch();

		tools.gotoTopTen();		

		tools.startServicedYearBatch();

		test.stopTest();

		tools.gotoDataIntegrityChecker();
		
		tools.gotoDeDupTool();

//		tools.startTopTenBatch();

		tools.actionCancel();
	}
	
}