// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class GuestRegistrationHandlerTest {
	public static Account customer; //used
    public static Account othercustomer;
    public static Vehicle_Ownership__c vo;
    public static Vehicle_Ownership__c othervo;
    public static Vehicle_Ownership__c voclone;
    public static Vehicle_Ownership__c queriedVO;
    public static Asset__c v1;
    public static Asset__c v2;
    
     static testmethod void getHashedIdLookup(){
        queriedDataSetup();
        MyLexusWebService.GRHashLookupResponse wsResponse = GuestRegistrationHandler.getHashedIdLookup('33dd55918501e33bbfcfd1a59164158d5b7a5ec63f2ce4173efe78653465581b', null, null);
        system.assertEquals(true, wsResponse.success);
        system.assertEquals(vo.Id, wsResponse.sfVOId);
        system.assertEquals(vo.Customer__c, wsResponse.sfAcctId);
    }

    
    static testmethod void guestRegistrationRequiredFields(){
        queriedDataSetup();
        MyLexusWebService.GRResponse wsoutput = 
        GuestRegistrationHandler.guestRegistration(null, null, null, null, null, null, null);
        system.assertEquals('700', wsoutput.errCode);
        wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, null, customer.Id, null);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void guestRegistrationMagicLinkExpired(){
        queriedDataSetup();
        //registration link expired
        vo.MagicLink_Sent_Date__c = system.today()-20;
        update vo;
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, vo.Id);
        system.assertEquals('614', wsoutput.errCode);
    }
    
    static testmethod void guestRegistrationAcctVOIdValid(){
        queriedDataSetup();
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, vo.Id);
        system.assertEquals(true, wsoutput.success );
        system.assertEquals(customer.Id, wsoutput.recordId );
        
        wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, vo.Id);
        system.assertEquals(true, wsoutput.success);
        // no new record will be created
        system.assertEquals(true, wsoutput.success );
        system.assertEquals(customer.Id, wsoutput.recordId );
        // check Stripe ID // added 20 November
        
        
        // 610 An account is found with GR records but no Gigya ID
        String gigya = customer.Guest_Gigya_ID__c;
        customer.Guest_Gigya_ID__c = null;
        update customer;
        wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, gigya, customer.Id, vo.Id);
        system.assertEquals(false, wsoutput.success );
        system.assertEquals('610', wsoutput.errCode );
        
    }
    
    static testmethod void guestRegistrationAcctVOIdNotMatch(){
        queriedDataSetup();
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, othervo.Id);
        // 601 SF acct ID and SF VO ID provided but they do not match
        system.assertEquals(false, wsoutput.success );
        system.assertEquals('601', wsoutput.errCode );
    }
    
    static testmethod void guestRegistrationAcctInactiveVOId(){
        queriedDataSetup();
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, othervo.Id);
        // 601 SF acct ID and SF VO ID provided but they do not match
        system.assertEquals(false, wsoutput.success );
        system.assertEquals('601', wsoutput.errCode );
    }
    
    static testmethod void guestRegistrationAcctVOIdNotValid(){
        queriedDataSetup();
        Id tempVoID = othervo.Id;
        othervo.Magic_Link_Sent__c = false;
        update othervo;
        List<VO_Offer_Detail__c> voodList = [select Id from VO_Offer_Detail__c where Vehicle_Ownership__c = :othervo.Id];
        for (VO_Offer_Detail__c vood : voodList){
            vood.Delete_Record__c = true;
        }
        update voodList;
        delete voodList;
        delete othervo;
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, tempVoID);
        // 602 VO Id provided is invalid or does not exist
        system.assertEquals(false, wsoutput.success );
        system.assertEquals('602', wsoutput.errCode );
    }
    
    static testmethod void guestRegistrationFindMatchingAccount(){
        queriedDataSetup();
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(customer.FirstName, customer.LastName, customer.PersonEmail, customer.PersonMobilePhone, customer.Guest_Gigya_ID__c, null, null);
        // Has a match
        system.assertEquals(true, wsoutput.success );
        system.assertEquals(customer.Id, wsoutput.recordId );
        system.assertEquals(customer.firstName, wsoutput.firstName );
    }
    
    static testmethod void guestRegistrationNoMatchonGigya(){
        queriedDataSetup();
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(customer.FirstName, customer.LastName, customer.PersonEmail, customer.PersonMobilePhone, 'BRANDNEWGIGYA6', null, null);
        // 603 Account matched but Gigya ID provided is not the same as the gigya Id in the account
        system.assertEquals(false, wsoutput.success );
        system.assertEquals('603', wsoutput.errCode );
    }
    
    static testmethod void guestRegistrationFindMatchAccount(){
        queriedDataSetup();
        String newGigya = 'NEWGIGYA3333';
        customer.Guest_Gigya_ID__c = null;
        update customer;
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(customer.FirstName, customer.LastName, customer.PersonEmail, customer.PersonMobilePhone + 'new' , newGigya, null, null);
        
        system.assertEquals(true, wsoutput.success );
        system.assertEquals(customer.Id, wsoutput.recordId );
        Account resultAcct = [select Id, Guest_Gigya_ID__c, Stripe_Customer_ID__c, Guest_Provided_Mobile__c from Account where Id = :customer.Id limit 1];
        //system.assertEquals(newGigya, resultAcct.Guest_Gigya_ID__c );
        //system.assertEquals(customer.PersonMobilePhone + 'new', resultAcct.Guest_Provided_Mobile__c );
        
        //An account is found with GR records but no Gigya ID
        customer.Guest_Gigya_ID__c = null;
        update customer;
        wsoutput = GuestRegistrationHandler.guestRegistration(customer.FirstName, customer.LastName, customer.PersonEmail, customer.PersonMobilePhone, newGigya, null, null);
        //system.assertEquals(false, wsoutput.success );
        //system.assertEquals('610', wsoutput.errCode );
        
    }
    
    static testmethod void guestRegistrationMultipleAccts(){
        queriedDataSetup();
        Account ac1 = new Account(FirstName = 'Harold', LastName = 'Harold', PersonEmail = 'harry@harry.com');
        List<Account> acList = new List<Account>();
        acList.add(ac1);
        Account ac2 = new Account(FirstName = 'Harold', LastName = 'Harold', PersonEmail = 'harry@harry.com');
        acList.add(ac2);
        insert acList;
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(ac1.FirstName, ac1.LastName, ac1.PersonEmail, null, 'BRANDNEWGIGYA', null, null);
        // 604 Multiple Account Matches on provided information
        system.debug(LoggingLevel.ERROR, '## ERROR CODE: ' + wsoutput.errCode);
        system.assertEquals(false, wsoutput.success );
        system.assertEquals('604', wsoutput.errCode );
    }
    
    static testmethod void guestRegistrationNoMatch(){
        queriedDataSetup();
        update customer;
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration('Sally', 'No Match', 'sally@sally.com', '11111', 'GIGYASALLY4', null, null);
        // 604 Multiple Account Matches on provided information
        system.assertEquals(true, wsoutput.success );
        system.assertEquals(true, wsoutput.recordId != null );
    }
    
  //  static testmethod void guestRegistrationVOInactive(){
    //    queriedDataSetup();
      //  voclone = vo.clone(false, false, false, false);
		//insert voclone;

        //MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, vo.Id);
        
        //system.assertEquals(true, wsoutput.success );
        //system.assertEquals(customer.Id, wsoutput.recordId );
        
        //vo.MagicLink_Sent_Date__c = system.today() - 20;
        //update vo;
        //wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, vo.Id); // for expired
        //system.assertEquals(false, wsoutput.success );
        //system.assertEquals('614', wsoutput.errCode );
        
    //}
    
    static testmethod void vehicleAssociationRequiredFields(){
    	MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleAssociation(null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void vehicleAssociationVinNotExist(){
    	queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleAssociation(customer.Id, 'NOTEXISTVIN');
        // 605 VIN does not exist
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('605', wsoutput.errCode);
    }
    
    static testmethod void vehicleAssociationVinNotAssigned(){
    	queriedDataSetup();
        Asset__c v3 = new Asset__c(Name = 'TOTALLYNEW');
        insert v3;
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleAssociation(customer.Id, 'TOTALLYNEW');
        // 608 VIN exists but is not associated with a customer.
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('608', wsoutput.errCode);
    }
    
    static testmethod void vehicleAssociationMatchSuccessful(){
    	queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleAssociation(customer.Id, v1.Name);
        // Success
        system.assertEquals(true, wsoutput.success);
        system.assertEquals(vo.Id, wsoutput.recordId);
    }
    
    static testmethod void vehicleAssociationMatchUnsuccessful(){
    	queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleAssociation(customer.Id, v2.Name);
        // 607 VIN has an active VO whose account does not match the guest account
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('607', wsoutput.errCode);
    }
    
    /*
    static testmethod void vehicleAssociationMultiple(){
    	DEV NOTES: We need to query a vo and another clone as we cannot create new VO's given SOQL limits at this point
		queriedDataSetup();
        vo.End_Date__c = null;
        vo.Status__c = 'Active';
        voclone.End_Date__c = null;
        voclone.Status__c = 'Active';
        List<Vehicle_Ownership__c> vList = new List<Vehicle_Ownership__c>();
        vList.add(vo);
        vList.add(voclone);
        update vList;
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleAssociation(customer.Id, v1.Name);
        // 606 Multiple VIN Matches found. Raised a case for investigation
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('606', wsoutput.errCode);
    }
    */
    
    static testmethod void vehicleVerificationRequiredFields(){
    	MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleVerification(null, null, null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void vehicleVerificationVinNotExist(){
    	queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleVerification(customer.Id, 'NOTEXISTVIN', 'BATCH', null);
        // 605 VIN does not exist
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('605', wsoutput.errCode);
    }
    
    static testmethod void vehicleVerificationVinNotAssigned(){
    	queriedDataSetup();
        Asset__c v3 = new Asset__c(Name = 'TOTALLYNEW');
        insert v3;
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleVerification(customer.Id, 'TOTALLYNEW', 'BATCH', null);
        // 608 VIN exists but is not associated with a customer.
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('608', wsoutput.errCode);
    }
    
    static testmethod void vehicleVerificationMatchSuccessful(){
    	queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleVerification(customer.Id, v1.Name, 'BATCH', null);
        // Success
        system.assertEquals(true, wsoutput.success);
        system.assertEquals(vo.Id, wsoutput.recordId);
    }
    
    static testmethod void vehicleVerificationMatchUnsuccessful(){
    	queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.vehicleVerification(customer.Id, v2.Name, 'BATCH', null);
        // 607 VIN has an active VO whose account does not match the guest account
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('609', wsoutput.errCode);
    }
    
    static testmethod void createUpdateCaseRequiredFields(){
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.createUpdateCase(null, null, null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void createUpdateCaseNoCaseNum(){
        queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.createUpdateCase('NOTEXISTINGCASENO', '0111111', '8 am', customer.Id);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('611', wsoutput.errCode);
    }
    
    static testmethod void createUpdateCaseNewCaseAndUpdate(){
        queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.createUpdateCase(null, '0111111', '8 am', customer.Id);
        system.assertEquals(true, wsoutput.success);
        Case resultCase = [select Id, CaseNumber, Callback_Phone__c, Time_Slot__c from Case where CaseNumber = :wsoutput.caseNumber limit 1];
        system.assertEquals('0111111', resultCase.Callback_Phone__c);
        system.assertEquals('8 am', resultCase.Time_Slot__c);
        // now call the service again to update
        wsoutput = GuestRegistrationHandler.createUpdateCase(resultCase.CaseNumber, '02222', '9 am', customer.Id);
        resultCase = [select Id, CaseNumber, Callback_Phone__c, Time_Slot__c from Case where CaseNumber = :wsoutput.caseNumber limit 1];
        system.assertEquals(true, wsoutput.success);
        system.assertEquals('02222', resultCase.Callback_Phone__c);
        system.assertEquals('9 am', resultCase.Time_Slot__c);
    }
    
    static testmethod void deleteRegistrationRequiredFields(){
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.deleteRegistration(null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void deleteRegistrationGigyaEmailNotMatch(){
        queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.deleteRegistration(customer.Guest_Gigya_ID__c, 'anotheremail@email.com');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('612', wsoutput.errCode);
    }
    
    static testmethod void deleteRegistrationGigyaDeleteSuccessful(){
        queriedDataSetup();
        system.assertEquals('GIGYA1111', customer.Guest_Gigya_ID__c);
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.deleteRegistration(customer.Guest_Gigya_ID__c, customer.PersonEmail);
        system.assertEquals(true, wsoutput.success);
        Account resultAcct = [select Id, Guest_Gigya_ID__c from Account where Id = :customer.Id limit 1];
        system.assertEquals(null, resultAcct.Guest_Gigya_ID__c);
    }
    
    static testmethod void updateTermsPoliciesRequiredFields(){
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.updateTermsPolicyVersion(null, null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void updateTermsPoliciesSuccessful(){
        queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.updateTermsPolicyVersion(customer.Id, 'TV1', 'PV1');
        system.assertEquals(true, wsoutput.success);
        Account resultAcct = [select Id, PrivacyPolicyVersion__c, TermsAndConditionsVersion__c from Account where Id = :customer.Id limit 1];
        system.assertEquals('TV1', resultAcct.TermsAndConditionsVersion__c);
        system.assertEquals('PV1', resultAcct.PrivacyPolicyVersion__c);
    }
    
    static testmethod void updateStripeRequiredFields(){
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.updateStripeCustomerId(null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }

	static testmethod void updateStripeSuccessful(){
        queriedDataSetup();
        MyLexusWebService.WSResponse wsoutput = GuestRegistrationHandler.updateStripeCustomerId(customer.Id, 'stripeNewId');
        system.assertEquals(true, wsoutput.success);
        Account resultAcct = [select Id, Stripe_Customer_ID__c from Account where Id = :customer.Id limit 1];
        system.assertEquals('stripeNewId', resultAcct.Stripe_Customer_ID__c);
    }
    
    private static void queriedDataSetup(){
        Id OWNERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        vo = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c where Vehicle_Type__c = 'New Vehicle' and Status__c = 'Active'
             limit 1];
        vo.Start_Date__c = system.today();
        vo.MagicLink_Sent_Date__c = system.today();
        vo.Magic_Link_Sent__c = true;
        vo.Hashed_ID__c ='33dd55918501e33bbfcfd1a59164158d5b7a5ec63f2ce4173efe78653465581b';
        update vo;
        /*voclone = vo.clone(false, false, false, false);
        insert voclone;*/
        othervo = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c where Vehicle_Type__c = 'New Vehicle' and Status__c = 'Active'
                   and Id != :vo.Id limit 1];
        customer = [select Id, FirstName, LastName, PersonEmail, PersonMobilePhone, Guest_Gigya_ID__c from Account
                   where RecordTypeId = :OWNERRECORDTYPEID and PersonEmail != null and Id = :vo.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'GIGYA1111';
        customer.Stripe_Customer_ID__c = 'testStripe111';
        update customer;
        v1 = [select Id, Name from Asset__c where Id = :vo.AssetID__c limit 1];
        v2 = [select Id, Name from Asset__c where Id != :vo.AssetID__c limit 1];
    }
}