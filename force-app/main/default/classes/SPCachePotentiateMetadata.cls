public with sharing class SPCachePotentiateMetadata
{
	private static SPCachePotentiateMetadata__c CachePotentiateMetadata;
	
	/***********************************************************************************************************
		Lookup Cached information
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	public static string getServiceLocation()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.ServiceLocation__c;
	}

	public static string getHotAlertRecordTypeID()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
	
		return CachePotentiateMetadata.HotAlertRecordTypeID__c;
	}

	public static string getOwnerDetailsRecordTypeID()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
	
		return CachePotentiateMetadata.OwnerDetailsRecordTypeID__c;
	}

	public static string getCaseOrigin()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
	
		return CachePotentiateMetadata.CaseOrigin__c;
	}

	public static string getSurveyTypeSales()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.SurveyTypeSales__c;
	}

	public static string getCaseTypeSales()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.CaseTypeSales__c;
	}

	public static string getSurveyTypeService()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.SurveyTypeService__c;
	}

	public static string getCaseTypeService()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.CaseTypeService__c;
	}

	public static string getSurveyTypeOwnerDetails()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.SurveyTypeOwnerDetails__c;
	}

	public static string getCaseTypeOwnerDetails()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.CaseTypeOwnerDetails__c;
	}

	public static string getCaseTypeUpdateDetails()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.CaseTypeUpdateDetails__c;
	}

	public static string getPotentiateUserID()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.PotentiateUserID__c;
	}

	public static string getCaseDefaultOwner()
	{
		if (CachePotentiateMetadata == null)
			LoadMetadata(); 
			
		return CachePotentiateMetadata.CaseDefaultOwner__c;
	}


	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		CachePotentiateMetadata = SPCachePotentiateMetadata__c.getInstance();
	}


	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void TestSPCachePotentiateMetadata()
	{
		SPCachePotentiateMetadata.getServiceLocation();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getHotAlertRecordTypeID();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getOwnerDetailsRecordTypeID();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getCaseOrigin();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getSurveyTypeSales();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getCaseTypeSales();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getSurveyTypeService();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getCaseTypeService();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getCaseTypeOwnerDetails();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getCaseTypeUpdateDetails();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getSurveyTypeOwnerDetails();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getPotentiateUserID();
		CachePotentiateMetadata = null;

		SPCachePotentiateMetadata.getCaseDefaultOwner();
		CachePotentiateMetadata = null;
	}
}