@isTest
public class SP_LeadManagementTest 
{
	private static Lead CreateTestLead(Enform_User__c enformUser)
	{
		Account TestEUOwnerAccount = new Account();
		TestEUOwnerAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		TestEUOwnerAccount.LastName				= 'TestLast1';
		TestEUOwnerAccount.PersonEmail			= 'test1@email.com';
		TestEUOwnerAccount.PersonMobilePhone = '12335';	
		
		insert TestEUOwnerAccount;
		
		Lead TestLead = new Lead();
		TestLead.FirstName = 'FTest';
		TestLead.LastName				= 'TestLast1';
		TestLead.Email			= 'test1@email.com';
		TestLead.MobilePhone = '12335';	
		insert TestLead;
		
		if (enformUser != null)
		{
			enformUser.Owner_Id__c = TestEUOwnerAccount.Id;
			enformUser.Lead_ID__c = TestLead.Id;
			
			insert enformUser;
		}
		
		return TestLead;
	}
	
	
	private static Enform_User__c CreateTestEnformUser()
	{
		Enform_User__c testEU = new Enform_User__c(Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last',
												   IsPrimaryUser__c = true);
		return testEU;
	}
   
   	// Logged In user is not Intelematics
	// Lead related to enform user - Update
	
	// Result
	
	// Lead updated
	// The given line can bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase001() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Lead testLead = CreateTestLead(enfUser);
    	
    	testLead.FirstName = 'Ftest';
    	testLead.LastName = 'Test3';
		
		update testLead;
		
		Test.stopTest();

		Lead ld = [select LastName from Lead where Id = :testLead.Id];
		system.assertEquals(ld.LastName, testLead.LastName);   	
    }  
    
    // Logged In user is not Intelematics
	// Lead related to enform user - Delete
	
	// Result
	
	// Lead Deleted
	// The given line can bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase002() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Lead testLead = CreateTestLead(enfUser);
    	
    	ID delID = testLead.Id;
		
		delete testLead;
		
		Test.stopTest();

		List<Lead> leads = [select Id from Lead where Id = :delID];
		system.assertEquals(leads.size(), 0);   	
    }
    
    // Logged In user is not Intelematics
	// Lead related to enform user - Update - No change to mandatory fields
	
	// Result
	
	// Lead updated
	// The given line can not bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase003() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Lead testLead = CreateTestLead(enfUser);
    	
    	testLead.Title = 'Mr';
		
		update testLead;
		
		Test.stopTest();

		Lead ld = [select Title from Lead where Id = :testLead.Id];
		system.assertEquals(ld.Title, testLead.Title);   	
    }
    
    // Logged In user is not Intelematics
	// Lead not related to enform user - Update
	
	// Result
	
	// Lead updated
	// The given line can not be seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase004() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Lead testLead = CreateTestLead(null);
    	
    	testLead.LastName = 'Test3';
		
		update testLead;
		
		Test.stopTest();

		Lead ld = [select LastName from Lead where Id = :testLead.Id];
		system.assertEquals(ld.LastName, testLead.LastName);    	
    }
    
    // Logged In user is not Intelematics
	// Lead not related to enform user - Delete
	
	// Result
	
	// Lead Deleted
	// The given line can not bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase005() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Lead testLead = CreateTestLead(null);
    	
    	ID delID = testLead.Id;
		
		delete testLead;
		
		Test.stopTest();

		List<Lead> leads = [select Id from Lead where Id = :delID];
		system.assertEquals(leads.size(), 0);   	
    }
    
   	// Logged In user is  Intelematics
	// Lead related to enform user - update
	
	// Result
	
	// Lead updated
	// The given line can not bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase006() 
    {
       	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Lead testLead = CreateTestLead(enfUser);
    	    	
    		SPCacheLexusEnformMetadata__c metaData = [select Id,Intelematics_User__c from SPCacheLexusEnformMetadata__c];
    		User currUser = [select Id from User where Id = :metaData.Intelematics_User__c];
    	
       	Test.startTest();
    	
    	system.runAs(currUser)
			{
    	testLead.LastName = 'Test3';
		
		try
		{
		update testLead;
		}
		catch(Exception e)
		{
			system.debug(e.getMessage());
		}
			}
		
		Test.stopTest();

		//Lead ld = [select LastName from Lead where Id = :testLead.Id];
		//system.assertEquals(ld.LastName, testLead.LastName);   
		
		//system.assertEquals(metaData.Intelematics_User__c, SPCacheLexusEnformMetadata.getIntelematicsUser());	
    }	        	     	               
}