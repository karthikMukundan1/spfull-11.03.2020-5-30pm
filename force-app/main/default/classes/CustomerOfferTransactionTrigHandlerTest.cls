// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class CustomerOfferTransactionTrigHandlerTest {
	static Account partnerAcct;
    static Offer__c activeOffer;
    static Vehicle_Ownership__c validForOfferVO;
    static Customer_Offer_Detail__c cood;
    static Asset__c asset;
    static Account customer;
    
    static testmethod void testDistributeVouchers(){
        dataSetup();
        Customer_Offer_Transaction__c ot = new Customer_Offer_Transaction__c ();
        ot.CustomerOffer__c = cood.Id;
        ot.Offer__c = activeOffer.Id;
        ot.Redemption_Type__c = cood.Offer_Type__c;
        ot.Redemption_Location__c = 'Test Location';
        ot.Redemption_Credits__c = 5;
        ot.Redemption_Start_Date_Time__c = system.now();
        ot.Redemption_End_Date_Time__c = system.now() + 1;
        ot.Redemption_Model__c = 'Test Model';
        ot.Redemption_Description__c = 'Test Desc';
        insert ot;
        
        Customer_Offer_Transaction__c ot2 = new Customer_Offer_Transaction__c ();
        ot2.CustomerOffer__c = cood.Id;
        ot2.Offer__c = activeOffer.Id;
        ot2.Redemption_Type__c = cood.Offer_Type__c;
        ot2.Redemption_Location__c = 'Test Location';
        ot2.Redemption_Credits__c = 10;
        ot2.Redemption_Start_Date_Time__c = system.now();
        ot2.Redemption_End_Date_Time__c = system.now() + 1;
        ot2.Redemption_Model__c = 'Test Model';
        ot2.Redemption_Description__c = 'Test Desc';
        insert ot2;
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        List<Voucher__c> voucherList = [select Id from Voucher__c where RecordTypeId = :CUSTOMERVOUCHERRECORDTYPEID
                                       and (OfferTransaction__c = :ot.Id or OfferTransaction__c = :ot2.Id)];
        
        system.assertEquals(2, voucherList.size());
    }
    
    static void dataSetup(){
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        Account partnerAcct = new Account(Name = 'Partner Account', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        insert partnerAcct;
        
        Vehicle_Ownership__c validForOfferVO = [select Id, Customer__c, AssetID__c, AssetID__r.Name from Vehicle_Ownership__c 
                          where Status__c = 'Active' and AssetID__r.Name = 'TESTVIN' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        update validForOfferVO;
        
        activeOffer = new Offer__c(Partner_Account__c = partnerAcct.Id, Offer_Type__c = 'Car Hire', Status__c = 'Inactive', Offer_Class__c = 'Customer', Customer_Platinum_Limit__c = 10);
        insert activeOffer;
        activeOffer.Status__c = 'Active';
        update activeOffer;
        
        cood = [select Id, Offer_Type__c from Customer_Offer_Detail__c where Customer__c = :customer.Id and Offer__c = :activeOffer.Id limit 1];
        
        
    }
}