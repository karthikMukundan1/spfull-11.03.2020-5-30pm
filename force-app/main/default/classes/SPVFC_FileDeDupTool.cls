public with sharing class SPVFC_FileDeDupTool
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	public string					mp_strVIN				{get;set;}
	public string					mp_strLastName			{get;set;}
	public string					mp_strEmail				{get;set;}
	public string					mp_strPhone				{get;set;}
	public string					mp_strCity				{get;set;}
	public string					mp_strPostcode			{get;set;}
	public string					mp_strStreet			{get;set;}

	public string					mp_strFileName			{get;set;}
	transient public Blob			mp_strFileContent		{get;set;}
	public string					mp_strDescription		{get;set;}
	public boolean					mp_bNoAttachmentBody	{get;set;}

	public boolean					mp_bProcessOneRecord	{get;set;}
	public boolean					mp_bNoMatch				{get;set;}
	public boolean					mp_bProcessFile			{get;set;}
	public boolean					mp_bViewMatches			{get;set;}
	public boolean					mp_bViewNonMatches		{get;set;}

	public boolean					mp_bFireJob				{get;set;}

	public integer					mp_iBatchInterval		{get;set;}

	public boolean					m_bTestFlag				{get;set;}
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	public List<Account>			mp_liAccount			{get;set;}
	public List<SPData>				mp_liData				{get;set;}
	public List<string>				m_listData = new List<string>();
	public List<SPJobData>			mp_liJobs				{get;set;}

	integer count = 1;
	public Integer getCount()
	{
		return count;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class for data from the Spreadsheet file
	public class SPData
	{
		public string		AccountId			{get; set;}
		public string		FirstName			{get; set;}
		public string		LastName			{get; set;}
		public string		Email				{get; set;}
		public string		Phone				{get; set;}
		public string		City				{get; set;}
		public string		Postcode			{get; set;}
		public string		Street				{get; set;}
		public string		InputOrMatch		{get; set;}

		public SPData(string strAccountId, string strFirst, string strLast, string strEmail, string strPhone, string strCity, string strPostcode, string strStreet, string strInputOrMatch)
		{
			AccountId		= strAccountId;
			FirstName		= strFirst;
			LastName		= strLast;
			Email			= strEmail;
			Phone			= strPhone;
			City			= strCity;
			Postcode		= strPostcode;
			Street			= strStreet;
			InputOrMatch	= strInputOrMatch;
		}	
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class for Job data
	public class SPJobData
	{
		public string		JobId				{get; set;}
		public string		JobName				{get; set;}
		public string		Status				{get; set;}
		public string		InputCount			{get; set;}
		public string		MatchCount			{get; set;}

		public SPJobData()
		{
			
		}

		public SPJobData(string strJobId, string strJobName, string strStatus, string strInputCount, string strMatchCount)
		{
			JobId			= strJobId;
			JobName			= strJobName;
			Status			= strStatus;
			InputCount		= strInputCount;
			MatchCount		= strMatchCount;
		}
	}


	/***********************************************************************************************************
		Constructor and Init
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SPVFC_FileDeDupTool()
	{
		mp_bProcessOneRecord = false;
		mp_bProcessFile = false;
		mp_bViewMatches = false;
		mp_bViewNonMatches = false;
		mp_bFireJob		= false;
		m_bTestFlag		= false;
	}

	public Pagereference Init()
	{
		mp_liJobs = new List<SPJobData>();

		Duplicate_Check_Job__c[] arrJob =	[
												select	Id,
														Name,
														Active__c,
														DataList__c,
														NoMatchList__c,
														In_Error__c,
														Filename__c,
														Input_Count__c,
														Match_Count__c,
														Overflow__c
												from	Duplicate_Check_Job__c
												order by LastModifiedDate desc
												limit	20
											];
		
		for (Duplicate_Check_Job__c sJob : arrJob)
		{
			SPJobData sJobData = new SPJobData();

			sJobData.JobId				= sJob.Id;
			sJobData.JobName			= sJob.Name;

			if (sJob.Active__c)
			{
				sJobData.Status			= 'Processing';
			}
			else
			{
				sJobData.Status			= 'Completed';
			}
			
			if (sJob.In_Error__c)
			{
				sJobData.Status			= 'In Error';
			}

			sJobData.InputCount			= sJob.Input_Count__c.intValue().format();

			if (sJob.Overflow__c)
			{
				sJobData.MatchCount		= 'More than ' + sJob.Match_Count__c.intValue().format();
			}
			else
			{
				sJobData.MatchCount		= sJob.Match_Count__c.intValue().format();
			}

			mp_liJobs.add(sJobData);
		}
		
		return null;
		
	}


	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference actionMatchOne()
	{
		mp_bProcessOneRecord = true;
		
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference processOneRecord()
	{
		// Validate input first


/*
mp_strVIN
mp_strFirstName
mp_strLastName
mp_strEmail
mp_strPhone
mp_strCity
mp_strPostcode
mp_strStreet
*/

		mp_bNoMatch = false;

		// Call the customer match rules
		MatchingAccountV2 ma = new MatchingAccountV2();
		id idMatchingAccount = ma.customerMatchingV2	(
														mp_strVIN,
														'',
														mp_strLastName,
														mp_strEmail,
														mp_strPhone,
														mp_strCity,
														mp_strPostcode,
														mp_strStreet
													);

		if (idMatchingAccount == null)
		{
			mp_bNoMatch = true;
	
			return null;			
		}

		Account sAccount =	[
								select	Id,
										FirstName,
										LastName,
										PersonEmail,
										CM_Mobile__pc,
										PersonHomePhone,
										PersonOtherPhone,
										Work_Phone__c,
										Phone,
										ShippingCity,
										ShippingPostalCode,
										ShippingStreet,
										BillingCity,
										BillingPostalCode,
										BillingStreet
								from	Account
								where	Id = :idMatchingAccount
							];

		// Add the Account to a list for display
		mp_liAccount = new List<Account>();
		mp_liAccount.add(sAccount);

		// Concatenate values for the multiple phone number fields					
		string strPhone = 'None';
		if (sAccount.CM_Mobile__pc != null)
		{
			// Back up four places
			strPhone = strPhone.substring(0, strPhone.length() - 4);
			strPhone += sAccount.CM_Mobile__pc + ' / ';
		}
		if (sAccount.PersonHomePhone != null)
		{
			// Back up
			if (strPhone == 'None')
			{
				strPhone = strPhone.substring(0, strPhone.length() - 4);
			}
			else
			{
				strPhone = strPhone.substring(0, strPhone.length() - 3);
			}
			strPhone += sAccount.PersonHomePhone + ' / ';
		}
		if (sAccount.PersonOtherPhone != null)
		{
			// Back up
			if (strPhone == 'None')
			{
				strPhone = strPhone.substring(0, strPhone.length() - 4);
			}
			else
			{
				strPhone = strPhone.substring(0, strPhone.length() - 3);
			}
			strPhone += sAccount.PersonOtherPhone + ' / ';
		}
		if (sAccount.Work_Phone__c != null)
		{
			// Back up
			if (strPhone == 'None')
			{
				strPhone = strPhone.substring(0, strPhone.length() - 4);
			}
			else
			{
				strPhone = strPhone.substring(0, strPhone.length() - 3);
			}
			strPhone += sAccount.Work_Phone__c + ' / ';
		}
		if (sAccount.Phone != null)
		{
			// Back up
			if (strPhone == 'None')
			{
				strPhone = strPhone.substring(0, strPhone.length() - 4);
			}
			else
			{
				strPhone = strPhone.substring(0, strPhone.length() - 3);
			}
			strPhone += sAccount.Phone + ' / ';
		}

		if (strPhone != 'None')
		{
			strPhone = strPhone.substring(0, strPhone.length() - 3);
		}

		// Concatenate values for the two sets of Address fields
		string strCity = 'None';
		if (sAccount.ShippingCity != null)
		{
			// Back up four places
			strCity = strCity.substring(0, strCity.length() - 4);
			strCity += sAccount.ShippingCity + ' / ';
		}
		if (sAccount.BillingCity != null)
		{
			// Back up
			if (strCity == 'None')
			{
				strCity = strCity.substring(0, strCity.length() - 4);
			}
			else
			{
				strCity = strCity.substring(0, strCity.length() - 3);
			}
			strCity += sAccount.BillingCity + ' / ';
		}

		if (strCity != 'None')
		{
			strCity = strCity.substring(0, strCity.length() - 3);
		}

		string strPostalCode = 'None';
		if (sAccount.ShippingPostalCode != null)
		{
			// Back up four places
			strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 4);
			strPostalCode += sAccount.ShippingPostalCode + ' / ';
		}
		if (sAccount.BillingPostalCode != null)
		{
			// Back up
			if (strPostalCode == 'None')
			{
				strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 4);
			}
			else
			{
				strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 3);
			}
			strPostalCode += sAccount.BillingPostalCode + ' / ';
		}

		if (strPostalCode != 'None')
		{
			strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 3);
		}

		string strStreet = 'None';
		if (sAccount.ShippingStreet != null)
		{
			// Back up four places
			strStreet = strStreet.substring(0, strStreet.length() - 4);
			strStreet += sAccount.ShippingStreet + ' / ';
		}
		if (sAccount.BillingStreet != null)
		{
			// Back up
			if (strStreet == 'None')
			{
				strStreet = strStreet.substring(0, strStreet.length() - 4);
			}
			else
			{
				strStreet = strStreet.substring(0, strStreet.length() - 3);
			}
			strStreet += sAccount.BillingStreet + ' / ';
		}

		if (strStreet != 'None')
		{
			strStreet = strStreet.substring(0, strStreet.length() - 3);
		}


		mp_liData = new List<SPData>();

		SPData sSPData = new SPData	(	sAccount.Id,
										sAccount.FirstName,
										sAccount.LastName,
										sAccount.PersonEmail,
										strPhone,
										strCity,
										strPostalCode,
										strStreet,
										''
									);

		mp_liData.add(sSPData);

		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference actionNewFile()
	{
		// Check to see if there is already an active job
		integer iActiveJobs =	[
									select	count()
									from	Duplicate_Check_Job__c
									where	Active__c = true
									and		In_error__c = false
								];
		if (iActiveJobs > 0)
       {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
            'A Duplicate Check Job is already running - cannot start another until it has completed');
            ApexPages.addMessage(msg);
            return null;
        }

		mp_bProcessFile = true;
		
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference processFile()
	{
		// Validate input first
		mp_bNoAttachmentBody = false;
		
        if (mp_strFileContent == null)
        {
            mp_bNoAttachmentBody = true;
            return null;
        }

		// Create new Duplicate Check Job record
		Duplicate_Check_Job__c sJob = new Duplicate_Check_Job__c();
		sJob.Name			=	mp_strFileName + ' - ' + 
								datetime.now().day() + '/' +
								datetime.now().month() + '/' +
								datetime.now().year() + ' at ' +
								datetime.now().hour() + ':' +
								datetime.now().minute() + ':' +
								datetime.now().second();
		sJob.Filename__c	= mp_strFileName;
		sJob.Input_Count__c	= 0;
		sJob.Match_Count__c	= 0;
		sJob.DataList__c = 'STRT|';
		sJob.NoMatchList__c = 'STRT|';
		sJob.Active__c		= true;

        try
        {
			insert sJob;
        }
        catch(DmlException ex)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
            'The Duplicate Check Job could not be created. Please review your input and resubmit');
            ApexPages.addMessage(msg);
            return null;
        }


       // Link file to the Job
        Attachment sAttachment          = new Attachment();
        sAttachment.Body                = mp_strFileContent;
        sAttachment.Name                = mp_strFileName;
        sAttachment.Description         = mp_strDescription;
        sAttachment.ParentId            = sJob.Id;

        try
        {
            insert sAttachment;
        }
        catch(DmlException ex)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
            'The Attachment could not be created. Please review your input and resubmit');
            ApexPages.addMessage(msg);
            return null;
        }

		if (!m_bTestFlag)
		{
//			SP_DeDupCheckBatch objSP_DeDupCheckBatch = new SP_DeDupCheckBatch();
//			ID batchprocessid = Database.executeBatch(objSP_DeDupCheckBatch);
		}

		mp_bFireJob	 = true;

		mp_bProcessFile = false;

		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference viewMatches()
	{
		mp_bViewMatches = true;

		id idJobId = ApexPages.currentPage().getParameters().get('JobId');

		if (idJobId == null)
		{
			return null;
		}

		mp_liData = new List<SPData>();

		Duplicate_Check_Job__c sJob2 =	[
											select	Id,
													Active__c,
													DataList__c,
													NoMatchList__c,
													In_Error__c,
													Filename__c,
													Input_Count__c,
													Match_Count__c,
													Overflow__c
											from	Duplicate_Check_Job__c
											where	Id = :idJobId
										];

		m_listData = sJob2.DataList__c.split('[|]', 0);

		for (integer i = 0; i < m_listData.size(); i++)
		{
			list<string> li_Data = new list<string>(m_listData[i].split('[,]', 0));

			SPData sSPData = new SPData	(	li_Data[0],
											li_Data[1],
											li_Data[2],
											li_Data[3],
											li_Data[4],
											li_Data[5],
											li_Data[6],
											li_Data[7],
											li_Data[8]
										);

			mp_liData.add(sSPData);
		}

		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference viewNonMatches()
	{
		mp_bViewNonMatches = true;

		id idJobId = ApexPages.currentPage().getParameters().get('JobId');

		if (idJobId == null)
		{
			return null;
		}

		mp_liData = new List<SPData>();

		Duplicate_Check_Job__c sJob2 =	[
											select	Id,
													Active__c,
													DataList__c,
													NoMatchList__c,
													In_Error__c,
													Filename__c,
													Input_Count__c,
													Match_Count__c,
													Overflow__c
											from	Duplicate_Check_Job__c
											where	Id = :idJobId
										];

		m_listData = sJob2.NoMatchList__c.split('[|]', 0);

		for (integer i = 0; i < m_listData.size(); i++)
		{
			list<string> li_Data = new list<string>(m_listData[i].split('[,]', 0));

			SPData sSPData = new SPData	(	li_Data[0],
											li_Data[1],
											li_Data[2],
											li_Data[3],
											li_Data[4],
											li_Data[5],
											li_Data[6],
											li_Data[7],
											li_Data[8]
										);

			mp_liData.add(sSPData);
		}

		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference rerunMatch()
	{
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
        'Please do not click on this link again');
        ApexPages.addMessage(msg);
        return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference deleteJob()
	{
		id idJobId = ApexPages.currentPage().getParameters().get('JobId');

		Duplicate_Check_Job__c sJob =	[
											select	Id
											from	Duplicate_Check_Job__c
											where	Id = :idJobId
										];
        try
        {
			delete sJob;
        }
        catch(DmlException ex)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
            'The Duplicate Check Job could not be deleted');
            ApexPages.addMessage(msg);
            return null;
        }
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
        'The Duplicate Check Job has been deleted');
        ApexPages.addMessage(msg);

		pageReference pageRedirect;
		pageRedirect = new PageReference('/apex/SPVFP_FileDeDupTool');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference actionBack()
	{
		mp_bProcessOneRecord = false;
		mp_bProcessFile = false;
		mp_bViewMatches = false;
		mp_bViewNonMatches = false;
		
		pageReference pageRedirect;
		pageRedirect = new PageReference('/apex/SPVFP_FileDeDupTool');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public pageReference actionCancel()
	{
		pageReference pageRedirect;
		pageRedirect = new PageReference('/o');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public pageReference fireAnotherBatch()
	{
		// Check to see if there is already an active job
		system.debug('*** in fire method ***');

		integer iActiveJobs =	[
									select	count()
									from	Duplicate_Check_Job__c
									where	Active__c = true
								];
		if (iActiveJobs == 0)
		{
			mp_bFireJob = false;

			pageReference pageRedirect;
			pageRedirect = new PageReference('/apex/SPVFP_FileDeDupTool');
		   	pageRedirect.setRedirect(true);
   			return pageRedirect;
		}
		else
		{
			if (!m_bTestFlag)
			{
				SP_DeDupCheckBatch objSP_DeDupCheckBatch = new SP_DeDupCheckBatch();
		        ID batchprocessid = Database.executeBatch(objSP_DeDupCheckBatch);
			}
		}

		count++;

		return null;
	}


	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSPVFC_FileDeDupTool()
	{
		// Create test data
       Account acct1 = new Account(
									FirstName			= 'Test', 
									LastName			= 'testSPVFC_FileDeDupTool',
									PersonEmail			= 'test1@email.com',
									PersonMobilePhone	= '1111111111',
									PersonHomePhone		= '3333333333',
									PersonOtherPhone	= '4444444444',
									Work_Phone__c		= '5555555555',
									Phone				= '6666666666',
									ShippingCity		= 'Sydney',
									ShippingPostalCode	= '2010',
									ShippingStreet		= 'George',
									BillingCity			= 'Melbourne',
									BillingPostalCode	= '3000',
									BillingStreet		= 'Collins');
		acct1.RecordTypeId = SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		insert acct1;

		// Dummy a completed Duplicate Check Job record
		Duplicate_Check_Job__c sJob = new Duplicate_Check_Job__c();
		sJob.Name			=	'Test Job file';
		sJob.Filename__c	= 'TestFileName';
		sJob.Input_Count__c	= 1234;
		sJob.Match_Count__c	= 2;
		sJob.DataList__c = '001O0000003DUyw,ed,22,test@test.com,null,null,null,null,INPUT|' +
							'001O0000003DV56IAG,11,22,test@test.com,1111111111,Ultimo,2333, 579 harris street,MATCH';
		sJob.Active__c		= false;
		sJob.In_error__c	= false;
		insert sJob;


		// Now do the testing		
		SPVFC_FileDeDupTool fddt = new SPVFC_FileDeDupTool();
		fddt.Init();

		// First exercise the single search option
		fddt.actionMatchOne();

		// Not enough info to match
		fddt.mp_strLastName = 'testSPVFC_FileDeDupTool';
		fddt.processOneRecord();

		fddt.actionNewFile();

		fddt.m_bTestFlag = true;

        // Attachment is required!
        blob blobTestBlob               = blob.valueOf('This is a test Blob!');
        fddt.mp_strFileContent			= blobTestBlob;
        fddt.mp_strDescription			= 'Test Attachment Description';
        fddt.mp_strFileName				= 'Test Attachment Name';

		fddt.ProcessFile();

		// Try to fire a batch while one is already active
		fddt.fireAnotherBatch();

		// View the matches from the dummy Job record
		ApexPages.currentPage().getParameters().put('JobId', sJob.Id);
		fddt.viewMatches();

		// Delete the completed Job
		fddt.deleteJob();
		
		fddt.actionBack();	
			
		fddt.actionCancel();		
		
	}
	
}