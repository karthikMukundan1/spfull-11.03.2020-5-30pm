// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for  creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class MyLexusWebServiceTestSet2 {
    // DEVELOPER NOTES: These are just all coverage test methods; The assertion test methods are in the corresponding
    // test method classes for each of the handler classes
    public static Account customer;
    public static Vehicle_Ownership__c vo;
    public static Asset__c vehicle;
    public static Case c;
    
    static testmethod void deleteRegistration(){
        dataSetup();
        MyLexusWebService.deleteRegistration(customer.Guest_Gigya_ID__c, customer.PersonEmail);
    }
    
    static testmethod void getCustomerDetails(){
        dataSetup();
        MyLexusWebService.getCustomerDetails(customer.Guest_Gigya_ID__c, null, null);
    }
    
    static testmethod void writeCustomerDetails(){
        dataSetup();
        MyLexusWebService.GuestAccount inputAcct = new MyLexusWebService.GuestAccount();
        inputAcct.gigyaId = customer.Guest_Gigya_ID__c;
        inputAcct.firstName = 'Test';
        inputAcct.lastName = 'Test';
        MyLexusWebService.writeCustomerDetails(inputAcct);
    }
    
    
    private static void dataSetup(){
        Id OWNERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        customer = new Account(FirstName = 'Henry', LastName = 'Sample', PersonEmail = 'sample@henry.com',
                               Guest_Gigya_ID__c = 'GIGYA1111', RecordTypeId = OWNERRECORDTYPEID);
        insert customer;
        
        vehicle = new Asset__c(Name = 'VIN1111');
        insert vehicle;
        
        vo = new Vehicle_Ownership__c(Customer__c = customer.Id, AssetID__c = vehicle.Id, Start_Date__c = system.today(),
                                     Status__c = 'Active', Vehicle_Type__c = 'New Vehicle');
        insert vo;
        
        c = new Case(AccountId = customer.Id, Subject = 'Test');
        insert c;
        c = [select Id, CaseNumber from Case where Id = :c.Id limit 1];
    }
}