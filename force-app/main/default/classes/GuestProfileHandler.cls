/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Handler class for all GuestProfile related methods.
 * WEB SERVICE USING THIS CLASS: MyLexusWebService
 * TEST CLASS: GuestProfileHandlerTest
 * 5/2/20 - Encore Tier Added - VD
 * 
 * */

global class GuestProfileHandler {
    global static MyLexusWebService.GuestProfileResponse getCustomerDetails (String gigyaId, Id sfAcctId, Id sfVOId){
        MyLexusWebService.GuestProfileResponse gpr = new MyLexusWebService.GuestProfileResponse();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (gigyaId == null || gigyaId == ''){
            if (sfAcctId == null && sfVOId == null){
                errDesc += 'gigyaId;';
            	requiredFieldsErrCounter++;
            }
            else {
                if (sfAcctId == null){
                    errDesc += 'sfAcctId;';
                	requiredFieldsErrCounter++;
                } 
                if (sfVOId == null){
                    errDesc += 'sfVOId;';
                	requiredFieldsErrCounter++;
                } 
            }
        }
        if (requiredFieldsErrCounter > 0){
            gpr.success = false;
            gpr.recordId = null;
            gpr.errCode = '700';
            gpr.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
        }
        else {
            List<Account> acctMatchList = new List<Account>();
            if (gigyaId != null && gigyaId != ''){
                acctMatchList = [select Id, Guest_Gigya_Id__c,
                                 GR_Status__c, FirstName, LastName, PersonEmail,
                                 PersonMobilePhone, ShippingStreet, ShippingCity,
                                 ShippingState, ShippingPostalCode, BillingStreet,
                                 BillingCity, BillingState, BillingPostalCode,
                                 Leisure_Time_Activities__pc, Do_Not_Contact__pc,
                                 Product_Information_Opt_Out__c, PersonDoNotCall,
                                 Benefits_Events_Opt_Out__c, PreferredDealerName__c, 
                                 Stripe_Customer_ID__c,
                                 //VD added for Account Type Change 5/2/20
                                 (select Encore_Tier__c  from Guest_Relationships__r where IsActive__c = true)
                                 //VD end Change 5/2/20
                                 from Account
                                 where Guest_Gigya_Id__c = :gigyaId limit 1];
            }
            else { // if gigya is not provided and just account id and voi id
                // check if account ID of VO matches the provided account Id
                List<Vehicle_Ownership__c> voList = [select Id, Customer__c, MagicLink_Expired__c, Customer__r.Guest_Gigya_ID__c from Vehicle_Ownership__c where Id = :sfVOId];
                if (voList.size() == 0){
                    // Invalid VO Id or no VO found 602 
                    gpr.success = false;
                    gpr.recordId = null;
                    gpr.errCode = '602';
                    gpr.errorDesc = 'VO Id provided is invalid or does not exist';
                    Case returnedCase = createErrorCase(null, null, gigyaId, '602 - ' + gpr.errorDesc);
                    gpr.caseNumber = returnedCase.caseNumber;
                    return gpr;
                }
                else {
                    // check if account matches
                    if (voList[0].Customer__c != sfAcctId){
                        // 601 SF acct ID and SF VO ID provided but they do not match
                        gpr.success = false;
                        gpr.recordId = null;
                        gpr.errCode = '601';
                        gpr.errorDesc = 'SF acct ID and SF VO ID provided but they do not match';
                        Case returnedCase = createErrorCase(null, null, gigyaId, '601 - ' + gpr.errorDesc);
                        gpr.caseNumber = returnedCase.caseNumber;
                        return gpr;
                    }
                    else { // if it matches
                        // check if Magic Link is expired
                        if (voList[0].MagicLink_Expired__c){ // if it is expired
                            // 614 Registration link expired
                            gpr.success = false;
                            gpr.recordId = null;
                            gpr.errCode = '614';
                            gpr.errorDesc = 'Registration link expired';
                            Case returnedCase = createErrorCase(null, null, gigyaId, '614 - ' + gpr.errorDesc);
                            gpr.caseNumber = returnedCase.caseNumber;
                            return gpr;
                        }
                        else {
                            acctMatchList = [select Id, Guest_Gigya_Id__c,
                                 GR_Status__c, FirstName, LastName, PersonEmail,
                                 PersonMobilePhone, ShippingStreet, ShippingCity,
                                 ShippingState, ShippingPostalCode, BillingStreet,
                                 BillingCity, BillingState, BillingPostalCode,
                                 Leisure_Time_Activities__pc, Do_Not_Contact__pc,
                                 Product_Information_Opt_Out__c, PersonDoNotCall,
                                 Benefits_Events_Opt_Out__c, PreferredDealerName__c,
                                 Stripe_Customer_ID__c, 
                                 //VD 5/2/20- Added Encore Tier Field
                                 (select Id,Encore_Tier__c from Guest_Relationships__r where IsActive__c = true)
                                 //VD 5/2/20 End.
                                 from Account
                                 where Id = :sfAcctId limit 1];
                            if (voList[0].Customer__r.Guest_Gigya_ID__c == null && acctMatchList[0].Guest_Relationships__r.size() >0){
                                // has existing GR
                                gpr.success = false;
                                gpr.errCode = '610';
                                gpr.errorDesc = 'An account is found with GR records but no Gigya ID';
                               	Case returnedCase = createErrorCase(voList[0].Customer__c, voList[0].Id, null, 'ERR_CODE_610 - An account is found with GR records but no Gigya ID');
                                gpr.caseNumber = returnedCase.CaseNumber;
                                return gpr;
                                
                            }
                        }
                    }
                    
                }
            }
            if (acctMatchList.size() == 0){ // no match
                gpr.success = false;
                gpr.recordId = null;
                gpr.errCode = '615';
                gpr.errorDesc = 'No existing account with the gigya ID / sfAcctId provided';
                Case returnedCase = createErrorCase(null, null, gigyaId, '615 - ' + gpr.errorDesc);
                gpr.caseNumber = returnedCase.caseNumber;
            }
            else {
                Account a = acctMatchList[0];
                MyLexusWebService.GuestAccount ga = new MyLexusWebService.GuestAccount();
                ga.gigyaId = a.Guest_Gigya_Id__c;
                ga.customerTypeStatus = a.GR_Status__c;
                ga.firstName = a.FirstName;
                ga.lastName = a.LastName;
                ga.email = a.PersonEmail;
                ga.mobilePhone = a.PersonMobilePhone;
                ga.homeStreet = a.ShippingStreet;
                ga.homeSuburb = a.ShippingCity;
                ga.homeState = a.ShippingState;
                ga.homePostCode = a.ShippingPostalCode;
                ga.postalStreet = a.BillingStreet;
                ga.postalSuburb = a.BillingCity;
                ga.postalState = a.BillingState;
                ga.postalPostCode = a.BillingPostalCode;
                ga.interests = a.Leisure_Time_Activities__pc;
                ga.doNotCall = a.PersonDoNotCall;
                ga.doNotContact = a.Do_Not_Contact__pc;
                ga.productInfoOptOut = a.Product_Information_Opt_Out__c;
                ga.benefitEventsOptOut = a.Benefits_Events_Opt_Out__c;
                ga.preferredDealer = a.PreferredDealerName__c;
                ga.stripeCustomerId = a.Stripe_Customer_ID__c;
                if (a.Guest_Relationships__r.size() >0){
                    ga.encoreTier = a.Guest_Relationships__r[0].Encore_Tier__c;
                }else{ ga.encoreTier = ''; }
                //ga.encoreTier = a.Guest_Relationships__r[0].Encore_Tier__c;
                gpr.outputAcct = ga; 
                gpr.success = true;
                gpr.recordId = a.Id;
            }
        }
        return gpr;
    }
    
    global static MyLexusWebService.GuestProfileResponse writeCustomerDetails(MyLexusWebService.GuestAccount ga){
        MyLexusWebService.GuestProfileResponse gpr = new MyLexusWebService.GuestProfileResponse();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (ga == null){
            errDesc += 'inputAcct;';
            requiredFieldsErrCounter++;
        }
        else if (ga.gigyaId == null || ga.gigyaId == ''){
            errDesc += 'inputAcct.gigyaId;';
            requiredFieldsErrCounter++;
        }
        if (requiredFieldsErrCounter > 0){
            gpr.success = false;
            gpr.recordId = null;
            gpr.errCode = '700';
            gpr.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
        }
        else {
            List<Account> acctMatchList = [select Id, Guest_Gigya_Id__c,
                                           FirstName, LastName, PersonEmail,
                                           PersonMobilePhone, ShippingStreet, ShippingCity,
                                           ShippingState, ShippingPostalCode, BillingStreet,
                                           BillingCity, BillingState, BillingPostalCode,
                                           Leisure_Time_Activities__pc
                                          from Account
                                          where Guest_Gigya_Id__c = :ga.gigyaId limit 1];
            if (acctMatchList.size() == 0){ // no match
                gpr.success = false;
                gpr.recordId = null;
                gpr.errCode = '615';
                gpr.errorDesc = 'No existing account with the gigya ID provided';
                Case returnedCase = createErrorCase(null, null, ga.gigyaId, '615 - ' + gpr.errorDesc);
                gpr.caseNumber = returnedCase.caseNumber;
            }
            else {
                Boolean customerDetailsChanged = false;
                String fieldChanges = '';
                Account a = [select Id, Guest_Gigya_Id__c,
                                           FirstName, LastName, PersonEmail,
                                           PersonMobilePhone, ShippingStreet, ShippingCity,
                                           ShippingState, ShippingPostalCode, BillingStreet,
                                           BillingCity, BillingState, BillingPostalCode,
                                           Leisure_Time_Activities__pc
                                          from Account
                                          where Id = :acctMatchList[0].Id limit 1];
                a.Guest_Gigya_Id__c = ga.gigyaId;
                if (ga.firstName != null && ga.firstName != '' && a.FirstName != ga.firstName) {
                    fieldChanges += 'First Name: From ' + a.FirstName + ' to ' + ga.firstName + ' | '; 
                    a.FirstName = ga.firstName;
                    customerDetailsChanged = true;
                }
                if (ga.lastName != null && ga.lastName != '' && a.LastName != ga.lastName) {
                    fieldChanges += 'Last Name: From ' + a.LastName + ' to ' + ga.lastName + ' | '; 
                    a.LastName = ga.lastName;
                    customerDetailsChanged = true;
                }
                if (ga.email != null && ga.email != '' && a.PersonEmail != ga.email) {
                    fieldChanges += 'Email: From ' + a.PersonEmail + ' to ' + ga.email + ' | '; 
                    a.PersonEmail = ga.email;
                    customerDetailsChanged = true;
                }
                if (ga.mobilePhone != null && ga.mobilePhone != '' && a.PersonMobilePhone != ga.mobilePhone) {
                    fieldChanges += 'Mobile Phone: From ' + a.PersonMobilePhone + ' to ' + ga.mobilePhone + ' | '; 
                    a.PersonMobilePhone = ga.mobilePhone;
                    customerDetailsChanged = true;
                }
                if (ga.homeStreet != null && ga.homeStreet != '' && a.ShippingStreet != ga.homeStreet) {
                    fieldChanges += 'Home Street: From ' + a.ShippingStreet + ' to ' + ga.homeStreet + ' | '; 
                    a.ShippingStreet = ga.homeStreet;
                    customerDetailsChanged = true;
                }
                if (ga.homeSuburb != null && ga.homeSuburb != '' && a.ShippingCity != ga.homeSuburb) {
                    fieldChanges += 'Home Suburb: From ' + a.ShippingCity + ' to ' + ga.homeSuburb + ' | '; 
                    a.ShippingCity = ga.homeSuburb;
                    customerDetailsChanged = true;
                }
                if (ga.homeState != null && ga.homeState != '' && a.ShippingState != ga.homeState) {
                    fieldChanges += 'Home State: From ' + a.ShippingState + ' to ' + ga.homeState + ' | '; 
                    a.ShippingState = ga.homeState; 
                    customerDetailsChanged = true;
                }
                if (ga.homePostCode != null && ga.homePostCode != '' && a.ShippingPostalCode != ga.homePostCode) {
                    fieldChanges += 'Home Post Code: From ' + a.ShippingPostalCode + ' to ' + ga.homePostCode + ' | '; 
                    a.ShippingPostalCode = ga.homePostCode;
                    customerDetailsChanged = true;
                }
                
                if (ga.postalSameAsHome){
                    if (ga.homeStreet != null && ga.homeStreet != '' && a.BillingStreet != ga.homeStreet) {
                        fieldChanges += 'Postal Street: From ' + a.BillingStreet + ' to ' + ga.homeStreet + ' | '; 
                        a.BillingStreet = ga.homeStreet;
                        customerDetailsChanged = true;
                    }
                    if (ga.homeSuburb != null && ga.homeSuburb != '' && a.BillingCity != ga.homeSuburb) {
                        fieldChanges += 'Postal Suburb: From ' + a.BillingCity + ' to ' + ga.homeSuburb + ' | '; 
                        a.BillingCity = ga.homeSuburb;
                        customerDetailsChanged = true;
                    }
                    if (ga.homeState != null && ga.homeState != '' && a.BillingState != ga.homeState) {
                        fieldChanges += 'Postal State: From ' + a.BillingState + ' to ' + ga.homeState + ' | '; 
                        a.BillingState = ga.homeState; 
                        customerDetailsChanged = true;
                    }
                    if (ga.homePostCode != null && ga.homePostCode != '' && a.BillingPostalCode != ga.homePostCode) {
                        fieldChanges += 'Postal Post Code: From ' + a.BillingPostalCode + ' to ' + ga.homePostCode + ' | '; 
                        a.BillingPostalCode = ga.homePostCode;
                        customerDetailsChanged = true;
                    }
                }
                else {
                    if (ga.postalStreet != null && ga.postalStreet != '' && a.BillingStreet != ga.postalStreet) {
                        fieldChanges += 'Postal Street: From ' + a.BillingStreet + ' to ' + ga.postalStreet + ' | '; 
                        a.BillingStreet = ga.postalStreet;
                        customerDetailsChanged = true;
                    }
                    if (ga.postalSuburb != null && ga.postalSuburb != '' && a.BillingCity != ga.postalSuburb) {
                        fieldChanges += 'Postal Suburb: From ' + a.BillingCity + ' to ' + ga.postalSuburb + ' | '; 
                        a.BillingCity = ga.postalSuburb;
                        customerDetailsChanged = true;
                    }
                    if (ga.postalState != null && ga.postalState != '' && a.BillingState != ga.postalState) {
                        fieldChanges += 'Postal State: From ' + a.BillingState + ' to ' + ga.postalState + ' | '; 
                        a.BillingState = ga.postalState; 
                        customerDetailsChanged = true;
                    }
                    if (ga.postalPostCode != null && ga.postalPostCode != '' && a.BillingPostalCode != ga.postalPostCode) {
                        fieldChanges += 'Postal Post Code: From ' + a.BillingPostalCode + ' to ' + ga.postalPostCode + ' | '; 
                        a.BillingPostalCode = ga.postalPostCode;
                        customerDetailsChanged = true;
                    }
                }
                if (ga.interests != null && ga.interests != '') a.Leisure_Time_Activities__pc = ga.interests;
                
                try {
                    update a;
                    // now get the updated Account
                    a = [select Id, Guest_Gigya_Id__c,
                                           FirstName, LastName, PersonEmail,
                                           PersonMobilePhone, ShippingStreet, ShippingCity,
                                           ShippingState, ShippingPostalCode, BillingStreet,
                                           BillingCity, BillingState, BillingPostalCode,
                                           Leisure_Time_Activities__pc
                                          from Account
                                          where Id = :a.Id limit 1];
                    MyLexusWebService.GuestAccount gaoutput = new MyLexusWebService.GuestAccount();
                    gaoutput.gigyaId = a.Guest_Gigya_Id__c;
                    gaoutput.firstName = a.FirstName;
                    gaoutput.lastName = a.LastName;
                    gaoutput.email = a.PersonEmail;
                    gaoutput.mobilePhone = a.PersonMobilePhone;
                    gaoutput.homeStreet = a.ShippingStreet;
                    gaoutput.homeSuburb = a.ShippingCity;
                    gaoutput.homeState = a.ShippingState;
                    gaoutput.homePostCode = a.ShippingPostalCode;
                    gaoutput.postalStreet = a.BillingStreet;
                    gaoutput.postalSuburb = a.BillingCity;
                    gaoutput.postalState = a.BillingState;
                    gaoutput.postalPostCode = a.BillingPostalCode;
                    gaoutput.interests = a.Leisure_Time_Activities__pc;
                    gaoutput.postalSameAsHome = ga.postalSameAsHome;
                    gpr.outputAcct = gaoutput; 
                    gpr.success = true;
                	gpr.recordId = a.Id;
                    if (customerDetailsChanged){ // if customer details are changed
                        gpr.caseNumber = ownerUpdateCase(a.Id, fieldChanges);
                    }
                }
                catch (DmlException e){
                    gpr.success = false;
                    gpr.recordId = null;
                    gpr.errCode = '800';
                    gpr.errorDesc = 'System error updating Guest Profile: ' + e.getMessage();
                    Case returnedCase = createErrorCase(null, null, ga.gigyaId, gpr.errorDesc);
                    gpr.caseNumber = returnedCase.CaseNumber;
                }
            }
        }
        return gpr;
    }
    
    global static MyLexusWebService.GuestProfileResponse writeCommunicationPreferences(MyLexusWebService.GuestAccount ga){
    	MyLexusWebService.GuestProfileResponse gpr = new MyLexusWebService.GuestProfileResponse();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (ga == null){
            errDesc += 'inputAcct;';
            requiredFieldsErrCounter++;
        }
        else if (ga.gigyaId == null || ga.gigyaId == ''){
            errDesc += 'inputAcct.gigyaId;';
            requiredFieldsErrCounter++;
        }
        if (requiredFieldsErrCounter > 0){
            gpr.success = false;
            gpr.recordId = null;
            gpr.errCode = '700';
            gpr.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
        }
        else {
            List<Account> acctMatchList = [select Id, Guest_Gigya_Id__c,
                                           Do_Not_Contact__pc,
                                           Product_Information_Opt_Out__c, PersonDoNotCall,
                                           Benefits_Events_Opt_Out__c, PreferredDealerName__c
                                          from Account
                                          where Guest_Gigya_Id__c = :ga.gigyaId limit 1];
            if (acctMatchList.size() == 0){ // no match
                gpr.success = false;
                gpr.recordId = null;
                gpr.errCode = '615';
                gpr.errorDesc = 'No existing account with the gigya ID provided';
                Case returnedCase = createErrorCase(null, null, ga.gigyaId, '615 - ' + gpr.errorDesc);
                gpr.caseNumber = returnedCase.caseNumber;
            }
            else {
                Account a = new Account (Id = acctMatchList[0].Id);
                if (ga.doNotContact != null) a.Do_Not_Contact__pc = ga.doNotContact;
                if (ga.productInfoOptOut != null) a.Product_Information_Opt_Out__c = ga.productInfoOptOut;
                if (ga.doNotCall != null) a.PersonDoNotCall = ga.doNotCall;
                if (ga.benefitEventsOptOut != null) a.Benefits_Events_Opt_Out__c = ga.benefitEventsOptOut;
                if (ga.preferredDealer != null && ga.preferredDealer != '') a.PreferredDealerName__c = ga.preferredDealer;
                
                try {
                    update a;
                    // now get the updated Account
                    a = [select Id, Guest_Gigya_Id__c,
                                 Do_Not_Contact__pc,
                                 Product_Information_Opt_Out__c, PersonDoNotCall,
                                 Benefits_Events_Opt_Out__c, PreferredDealerName__c
                                          from Account
                                          where Id = :a.Id limit 1];
                    MyLexusWebService.GuestAccount gaoutput = new MyLexusWebService.GuestAccount();
                    gaoutput.gigyaId = a.Guest_Gigya_Id__c;
                    gaoutput.doNotCall = a.PersonDoNotCall;
                    gaoutput.doNotContact = a.Do_Not_Contact__pc;
                    gaoutput.productInfoOptOut = a.Product_Information_Opt_Out__c;
                    gaoutput.benefitEventsOptOut = a.Benefits_Events_Opt_Out__c;
                    gaoutput.preferredDealer = a.PreferredDealerName__c;
                    gpr.outputAcct = gaoutput; 
                    gpr.success = true;
                	gpr.recordId = a.Id;
                }
                catch (DmlException e){
                    gpr.success = false;
                    gpr.recordId = null;
                    gpr.errCode = '800';
                    gpr.errorDesc = 'System error updating Guest Profile: ' + e.getMessage();
                    Case returnedCase = createErrorCase(null, null, ga.gigyaId, gpr.errorDesc);
                    gpr.caseNumber = returnedCase.CaseNumber;
                }
            }
            
            
        }
        return gpr;
    }
    
    private static Case createErrorCase(Id sfAcctId, Id sfVOId, String gigyaId, String caseCreateReason){
        Id GENERALENQUIRYRTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('General_Enquiry').getRecordTypeId();
        Case newCase = new Case(RecordTypeId = GENERALENQUIRYRTYPEID);
        newCase.Type = 'Data Verification';
        newCase.Origin = 'MyLexus';
        newCase.Status = 'In Progress';
        newCase.AccountId = sfAcctId;
        newCase.Subject = 'MyLexus Customer Issue';
        String description = caseCreateReason;
        description += ' | Account ID: ' + sfAcctId;
        description += ' | VO ID: ' + sfVOId;
        description += ' | Gigya ID: ' + gigyaId;
        newCase.Description = description;
        try {
            insert newCase;
            // get case number
            newCase = [select Id, CaseNumber from Case where Id = :newCase.Id limit 1];
            return newCase;
        }
        catch (DmlException e){
            system.debug(LoggingLevel.ERROR, '## Error inserting case: ' + e.getMessage());
            return null;
        }
        
    }
    
    private static String ownerUpdateCase(Id acctId, String fieldChanges){
        Id OWNERDETAILSRECORDTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Owner_Details').getRecordTypeId();
        Case c = new Case(RecordTypeId = OWNERDETAILSRECORDTYPEID);
        c.AccountId = acctId;
        // get contact Id
        List<Account> aList = [select Id, PersonContactId from Account where Id = :acctId limit 1];
        if (aList.size() > 0) {
        	c.ContactId = aList[0].PersonContactId;    
        }
        // get most recent vehicle Ownership
        List<Vehicle_Ownership__c> voList = [select Id from Vehicle_Ownership__c
                                            where Customer__c = :acctId and Status__c = 'Active'
                                            order by Start_Date__c desc];
        if (voList.size() > 0){
            c.Vehicle_Ownership__c = voList[0].Id;
        }
        
        
        c.Status = 'Closed';
        c.Origin = 'Web';
        c.Priority = 'Medium';
        c.Type = 'Update Details';
        c.Subject = 'Update Details from Ownership app';
        c.Description = fieldChanges;
        try {
            insert c;
            c = [select Id, CaseNumber from Case where Id = :c.Id limit 1];
        }
        catch (DmlException e){
            system.debug(LoggingLevel.ERROR, 'Error creating case: ' + e.getMessage());
            return null;
        }
        return c.CaseNumber;
    }
}