@isTest
private class SP_TestCMUtils_Basic_Pt4
{
//	This test class excercises cases 25 - 32 of the Customer Matching Rules
//	using the Basic call to the utility

    static testMethod void myTestCustomerMatchingUtils()
    {
		/*String strlastName = '';
		String strEmail = ''; 
		String strMobile = ''; 
		String strCity = ''; 
		String strPostcode = ''; 
		String strStreet = '';

		// Create some test data
		Asset__c asst = new Asset__c	(
											Name = 'ABC12345678'
										);
		insert asst;

		Asset__c asst2 = new Asset__c	(
											Name = 'ABC123DIFF8'
										);
		insert asst2;

		Account acct1 = new Account();
		acct1.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		acct1.LastName				= 'TestLast1';
		acct1.PersonEmail			= 'test1@email.com';
		acct1.PersonMobilePhone		= '194837465';
		acct1.ShippingCity			= 'Sydney';
		acct1.ShippingStreet		= 'George';
		acct1.ShippingPostalCode	= '2001';
		acct1.BillingCity			= 'Melbourne';
		acct1.BillingStreet			= 'Collins';
		acct1.BillingPostalCode		= '3001';

		insert acct1;



		Vehicle_Ownership__c vo = new Vehicle_Ownership__c	(
																Customer__c	= acct1.Id,
																AssetID__c = asst.Id
															);
		insert vo;*/

/*		Matching rules

		Action depends on the sequence/combination of field matches - same VIN, same email, same mobile, same last name
		and similar shipping address

					E	M
		T C			m	o	L	A
		e a		V	a	b	a	d
		s s		I	i	P	s	d
		t e		N	l	h	t	r	Action
		=========================================
		  1		Y	Y	Y	Y	Y	Customer match
		  2		Y	Y	Y	Y	N	Same customer - update address
		  3		Y	Y	Y	N	Y	Same customer - update name
		  4		Y	Y	Y	N	N	Raise Case
		  5		Y	Y	N	Y	Y	Same customer - update mobile
		  6		Y	Y	N	Y	N	Same customer - update address and mobile
		  7		Y	Y	N	N	Y	Same customer - update name and mobile
		  8		Y	Y	N	N	N	Raise Case
				....
		  9		Y	N	Y	Y	Y	Same customer - update email
		 10		Y	N	Y	Y	N	Same customer - update address and email
		 11		Y	N	Y	N	Y	Same customer - update name and email
		 12		Y	N	Y	N	N	Raise Case
		 13		Y	N	N	Y	Y	Customer match	<--- RULE CHANGE 02DEC10 was raise Case
		 14		Y	N	N	Y	N	Raise Case
		 15		Y	N	N	N	Y	Raise Case
		 16		Y	N	N	N	N	Different customer
				....
		 17		N	Y	Y	Y	Y	Customer match
		 18		N	Y	Y	Y	N	Same customer - update address
		 19		N	Y	Y	N	Y	Same customer - update name
		 20		N	Y	Y	N	N	Raise Case
		 21		N	Y	N	Y	Y	Same customer - update mobile
		 22		N	Y	N	Y	N	Same customer - update address and mobile
		 23		N	Y	N	N	Y	Raise Case
		 24		N	Y	N	N	N	Raise Case
				....
		 25		N	N	Y	Y	Y	Customer match
		 26		N	N	Y	Y	N	Same customer - update address and email
		 27		N	N	Y	N	Y	Raise Case
		 28		N	N	Y	N	N	Raise Case
		 29		N	N	N	Y	Y	Customer match	<--- RULE CHANGE 02DEC10 was raise Case
		 30		N	N	N	Y	N	Different customer
		 31		N	N	N	N	Y	Different customer
		 32		N	N	N	N	N	Different customer

*/

		/*// Test case 25
		string strResult25 = MatchingAccount_WS.MatchingAccount(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@different.com', '194837465', 'Melbourne', '3001', 'Collins');

		// Exact match
		system.debug('*** strResult25 ***' + strResult25);
		system.assertEquals(acct1.Id, strResult25, 'Test case 25 failed');

		// Test case 26
		string strResult26 = MatchingAccount_WS.MatchingAccount(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@different.com', '194837465', 'Hobart', '', '');

		// Check that address and email has been marked for change
		system.debug('*** strResult26 ***' + strResult26);
		system.assertEquals(acct1.Id, strResult26, 'Test case 26 failed');

		// Test case 27
		string strResult27 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@different.com', '194837465', 'Sydney', '2001', 'George', false);

		// Check that null is returned
		system.debug('*** strResult27 ***' + strResult27);
		system.assertEquals(null, strResult27, 'Test case 27 failed');

		// Test case 28
		string strResult28 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@different.com', '194837465', 'Hobart', '', '', false);

		// Check that null is returned
		system.debug('*** strResult28 ***' + strResult28);
		system.assertEquals(null, strResult28, 'Test case 28 failed');*/
/*
		// Test case 29
		string strResult29 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@different.com', '222222222', 'Sydney', '2001', 'George', false);

		// Check that a match is returned
		system.debug('*** strResult29 ***' + strResult29);
		system.assertEquals(acct1.Id, strResult29, 'Test case 29 failed');

		// Test case 30
		string strResult30 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@different.com', '222222222', 'Hobart', '', '', false);

		// Check that null is returned
		system.debug('*** strResult30 ***' + strResult30);
		system.assertEquals(null, strResult30, 'Test case 30 failed');

		// Test case 31
		string strResult31 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@different.com', '222222222', 'Melbourne', '3001', 'Collins', false);

		// Check that null is returned
		system.debug('*** strResult31 ***' + strResult31);
		system.assertEquals(null, strResult31, 'Test case 31 failed');

		// Test case 32
		string strResult32 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@different.com', '222222222', 'Hobart', '', '', false);

		// Check that null is returned
		system.debug('*** strResult32 ***' + strResult32);
		system.assertEquals(null, strResult32, 'Test case 32 failed');
*/

    }

}