/*******************************************************************************
@author:        Donnie Banez
@date:          August 2019
@description:   Trigger Handler for Asset Trigger
@Revision(s):    
@Test Methods:  AssetTriggerHandlerTest
********************************************************************************/
public with sharing class AssetTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.Asset__c; 
    public static Boolean createODRecordsHasRun = false;
    public static Boolean recalcCODHasRun = false;
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  						
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  						if (!createODRecordsHasRun) createCODVINODVOODRecords((Map<Id, Asset__c>) oldMap, newList);
                        if (!recalcCODHasRun) recalculateCounts((Map<Id, Asset__c>) oldMap, newList);
       				}
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
   
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){
                    if (isBefore){
                        
                    }
                    if (isAfter){
  
                    }
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### Offer Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
        
    }
    
    public static void createCODVINODVOODRecords(Map<Id, Asset__c> oldMap, List<Asset__c> newList){
        createODRecordsHasRun = true; // mark as ran in this transaction instance
        // Get those Assets that have changed Encore Tier
        Set<Id> vinIdSet = new Set<Id>();
        for (Asset__c a : newList){
            Asset__c oldAsset = null;
            if (oldMap != null){
                oldAsset = oldMap.get(a.Id);
            }
            if (oldAsset != null && (oldAsset.Encore_Tier__c != a.Encore_Tier__c || oldAsset.Encore_Expiration_Date__c != a.Encore_Expiration_Date__c ) && (a.Encore_Tier__c == 'Platinum' || a.Encore_Tier__c =='Basic')){
                vinIdSet.add(a.Id);
            }
        }
        
        if (vinIdSet.size() > 0){
            GuestVoucherServicesUtilities.createCODVINODVOODRecords(null, vinIdSet, null); // offerIdSet = null, vinIdSet, voIdSet = null 
        }
    }
    
    public static void recalculateCounts(Map<Id, Asset__c> oldMap, List<Asset__c> newList){
       	recalcCODHasRun = true;
        Set<Id> codIdSet = new Set<Id>();
        Set<Id> assetIdSet = new Set<Id>();
        for (Asset__c a : newList){
            Asset__c oldAsset = null;
            if (oldMap != null){
                oldAsset = oldMap.get(a.Id);
            }
            if (oldAsset.Original_Sales_Date__c != a.Original_Sales_Date__c){ // if date has changed recalculate COD
                assetIdSet.add(a.Id);
            }
        }
        
        List<Asset__c> aList = [select Id,
                               (select Id, CustomerOfferDetail__c from VO_Offer_Details__r
                               where CustomerOfferDetail__c != null)
                               from Asset__c where Id in :assetIdSet];
        
        for (Asset__c a : aList){
            for (VO_Offer_Detail__c vood : a.VO_Offer_Details__r){
                codIdSet.add(vood.CustomerOfferDetail__c);
            }
        }
        if (codIdSet.size() > 0){
            GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet);
        }
    }
}