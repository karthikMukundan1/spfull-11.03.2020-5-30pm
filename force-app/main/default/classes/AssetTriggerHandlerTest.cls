// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class AssetTriggerHandlerTest {
    public static Asset__c vin;
    static testmethod void updateAsset(){
        dataSetup();
        List<VIN_Offer_Detail__c> vinodList = [select Id from VIN_Offer_Detail__c where VIN__c = :vin.Id];
        //system.assertEquals(0, vinodList.size());
        vin.Encore_Tier__c = 'Platinum';
        vin.Original_Sales_Date__c = system.today();
        update vin;
        // check if VINOD records are created
        vinodList = [select Id from VIN_Offer_Detail__c where VIN__c = :vin.Id];
        system.assertEquals(true, vinodList.size() > 0);
    }
    
    private static void dataSetup(){
        Vehicle_Ownership__c validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and AssetID__r.Encore_Tier__c != null and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        vin = [select Id, Encore_Tier__c from Asset__c where Id = :validForOfferVO.AssetID__c limit 1];
        
    }
}