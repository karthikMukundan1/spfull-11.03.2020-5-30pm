// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for  creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class MyLexusWebServiceTestSet1 {
    // DEVELOPER NOTES: These are just all coverage test methods; The assertion test methods are in the corresponding
    // test method classes for each of the handler classes
    public static Account customer;
    public static Vehicle_Ownership__c vo;
    public static Asset__c vehicle;
    public static Case c;
    
    static testmethod void getHashedIdLookup(){
        dataSetup();
        MyLexusWebService.GRHashLookupResponse wsResp = MyLexusWebService.getHashedIdLookup('33dd55918501e33bbfcfd1a59164158d5b7a5ec63f2ce4173efe78653465581b', null, null);
    }
    
    static testmethod void guestRegistration(){
        dataSetup();
        MyLexusWebService.guestRegistration(customer.FirstName, customer.LastName, customer.PersonEmail, null, customer.Guest_Gigya_ID__c, customer.Id, vo.Id);
    }
    
    static testmethod void vehicleAssociation(){
        dataSetup();
        MyLexusWebService.vehicleAssociation(customer.Id, vehicle.Name);
    }
    
    static testmethod void vehicleVerification(){
        dataSetup();
        MyLexusWebService.vehicleVerification(customer.Id, vehicle.Name, 'Batch Number', vo.Id);
    }
    
    static testmethod void createUpdateCase(){
        dataSetup();
        MyLexusWebService.createUpdateCase(c.CaseNumber, '01111111', 'Test Time Slot', customer.Id);
    }
    
    static testmethod void writeCommunicationPreferences(){
        dataSetup();
        MyLexusWebService.GuestAccount inputAcctCommPref = new MyLexusWebService.GuestAccount();
        inputAcctCommPref.gigyaId = customer.Guest_Gigya_ID__c;
        inputAcctCommPref.homeStreet = 'home street';
        inputAcctCommPref.homeSuburb = 'home suburb';
        inputAcctCommPref.postalSameAsHome = true;
        MyLexusWebService.writeCommunicationPreferences(inputAcctCommPref);
    }
    
    static testmethod void updateTermsPolicyVersion(){
        dataSetup();
        MyLexusWebService.updateTermsPolicyVersion(customer.Id, 'TCV1', 'PPV1');
    }
    
    static testmethod void updateStripeCustomerId(){
        dataSetup();
        MyLexusWebService.updateStripeCustomerId(customer.Id, 'stripeTestId');
    }
    
    static testmethod void getGuestVehicles(){
        dataSetup();
        MyLexusWebService.GuestVehicleWSResponse resp = MyLexusWebService.getGuestVehicles('GIGYA1111');
        List<MyLexusWebService.GuestRelationshipVehicles> grVehiclesList = new List<MyLexusWebService.GuestRelationshipVehicles>();
		grVehiclesList = resp.grVehiclesList;      
    }
    
    private static void dataSetup(){
        Id OWNERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        customer = new Account(FirstName = 'Henry', LastName = 'Sample', PersonEmail = 'sample@henry.com',
                               Guest_Gigya_ID__c = 'GIGYA1111', RecordTypeId = OWNERRECORDTYPEID);
        insert customer;
        
        vehicle = new Asset__c(Name = 'VIN1111');
        insert vehicle;
        
        vo = new Vehicle_Ownership__c(Customer__c = customer.Id, AssetID__c = vehicle.Id, Start_Date__c = system.today(),
                                     Status__c = 'Active', Vehicle_Type__c = 'New Vehicle');
        insert vo;
        
        c = new Case(AccountId = customer.Id, Subject = 'Test');
        insert c;
        c = [select Id, CaseNumber from Case where Id = :c.Id limit 1];
        Guest_Relationship__c gr = new Guest_Relationship__c();
        gr.Account__c = customer.Id;
        gr.VehicleOwnership__c = vo.Id;
        gr.GR_Status__c = 'Verified';
        gr.Gigya_ID__c = 'GIGYA1111';
        insert gr;
    }
}