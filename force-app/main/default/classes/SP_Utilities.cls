public class SP_Utilities 
{
	public static boolean AccountUpdatedByEnformTrigger = false;
	public static boolean AccountUpdatedByVOTrigger = false;
	public static boolean AccountUpdatedByVOUpdateAccInfoTrigger = false;
	public static boolean AccountUpdatedByWarrantyTrigger = false;
	public static boolean AccountUpdatedByOwnerModelsTrigger = false;
	public static boolean AccountUpdatedByPMAllocationTrigger = false;
	public static boolean AccountUpdatedByCalcServValTrigger = false;
	public static boolean AccountUpdatedByServRegoAssetServDateTrigger = false;
	public static boolean AccountUpdatedBySurveyUpdOwnerTrigger = false;
	public static boolean AccountUpdatedByCaseOptOutTrigger = false;
	public static boolean AccountUpdatedByCaseSetSelTrigger = false;
		
	public static boolean EnformUpdatedByAccountTrigger = false;
	public static boolean EnformUpdatedByEnformTrigger = false;
	
	public static boolean LeadUpdatedByEnformTrigger = false;
	public static boolean EnformUpdatedByLeadTrigger = false;	
	
	public static boolean AssetUpdatedByVOUpdateAccInfoTrigger = false;
	public static boolean AssetUpdatedByVOChangeOwnershipTrigger = false;
	public static boolean AssetUpdatedByServRegoAssetServDateTrigger = false;
	
	public static string checkFieldChangeAllowed(Map<ID, sObject> map_NewSObjects, 
												  Map<ID, sObject> map_OldSObjects,
												  Set<String> set_FieldNames)
	{
		Schema.SObjectType objType = map_NewSObjects.values()[0].getSObjectType();
		
		map<String, String> map_FieldToLabel = new map<String, String>();
		for (Schema.SObjectField sObjField : objType.getDescribe().fields.getMap().values())
		{
			Schema.DescribeFieldResult sFieldResult = sObjField.getDescribe();
			
			if (sFieldResult.isCalculated() || 
				sFieldResult.getLocalName().toLowerCase() == 'lastmodifieddate' ||
				sFieldResult.getLocalName().toLowerCase() == 'systemmodstamp' ||
				sFieldResult.getLocalName().toLowerCase() == 'lastmodifiedbyid' ||
				sFieldResult.getLocalName().toLowerCase() == 'name'||
				sFieldResult.getLocalName().toLowerCase() == 'lastvieweddate')
				continue;
				
			system.debug('############## set_AllFieldNames ' + sFieldResult.getLocalName().toLowerCase());
				
			map_FieldToLabel.put(sFieldResult.getLocalName().toLowerCase(), sFieldResult.getLabel());
		}
		
		
		
		for (ID objId : map_NewSObjects.keySet())
		{
			SObject newObj = map_NewSObjects.get(objId);
			SObject oldObj = map_OldSObjects.get(objId);
					
			for (string fieldName : map_FieldToLabel.keySet())
			{
				if (newObj.get(fieldName) != oldObj.get(fieldName) && !set_FieldNames.contains(fieldName))
					return map_FieldToLabel.get(fieldName);
			}
		}
		
		return null;
	}
	
	public static string checkFieldChangeRestricted(Map<ID, sObject> map_NewSObjects, 
												  Map<ID, sObject> map_OldSObjects,
												  Set<String> set_FieldNames)
	{
		Schema.SObjectType objType = map_NewSObjects.values()[0].getSObjectType();
		
		map<String, String> map_FieldToLabel = new map<String, String>();
		for (Schema.SObjectField sObjField : objType.getDescribe().fields.getMap().values())
		{
			Schema.DescribeFieldResult sFieldResult = sObjField.getDescribe();
			
			if (sFieldResult.isCalculated() || 
				sFieldResult.getLocalName().toLowerCase() == 'lastmodifieddate' ||
				sFieldResult.getLocalName().toLowerCase() == 'systemmodstamp' ||
				sFieldResult.getLocalName().toLowerCase() == 'lastmodifiedbyid' ||
				sFieldResult.getLocalName().toLowerCase() == 'name'||
				sFieldResult.getLocalName().toLowerCase() == 'lastvieweddate')
				continue;
				
			system.debug('############## set_AllFieldNames ' + sFieldResult.getLocalName().toLowerCase());
				
			map_FieldToLabel.put(sFieldResult.getLocalName().toLowerCase(), sFieldResult.getLabel());
		}
		
		
		
		for (ID objId : map_NewSObjects.keySet())
		{
			SObject newObj = map_NewSObjects.get(objId);
			SObject oldObj = map_OldSObjects.get(objId);
					
			for (string fieldName : map_FieldToLabel.keySet())
			{
				if (newObj.get(fieldName) != oldObj.get(fieldName) && set_FieldNames.contains(fieldName))
					return map_FieldToLabel.get(fieldName);
			}
		}
		
		return null;
	}	
}