/*******************************************************************************
@author:        Donnie Banez
@date:          July 2019
@description:   Trigger Handler for Offer Transaction Trigger
@Revision(s):    
@Test Methods:  CustomerOfferTransactionTrigHandlerTest
********************************************************************************/
public with sharing class CustomerOfferTransactionTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.Offer_Transaction__c; 
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  						distributeVouchers(newList);
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        //preventCOTDeletion(oldList);
                    }            
                    // After
                    if(isAfter){
   
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){
                    if (isBefore){
                        
                    }
                    if (isAfter){
  
                    }
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### Offer Transaction Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
    }
    
    public static void distributeVouchers(List<Customer_Offer_Transaction__c> newList){
        // get VO Offer Details for the customer offer detail
        Id CUSTOMERVEHICLEOFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        Set<Id> coodIdSet = new Set<Id>();
        for (Customer_Offer_Transaction__c ot : newList){
            coodIdSet.add(ot.CustomerOffer__c);
        }
        List<Customer_Offer_Detail__c> coodList = [select Id,
                                                  (select Id, Vehicle_Ownership__c, Vin_Offer_Detail__c, Customer_Credits_Available__c from VO_Offer_Details__r
                                                  where RecordTypeId = :CUSTOMERVEHICLEOFFERRECORDTYPEID
                                                   and Customer_Credits_Available__c > 0
                                                  order by Voucher_Expiration__c asc, Vehicle_Ownership__r.CreatedDate asc)
                                                  from Customer_Offer_Detail__c where Id in :coodIdSet];
        
        Map<Id, List<VO_Offer_Detail__c>> coodIdVOODListMap = new Map<Id, List<VO_Offer_Detail__c>>();
        
        for (Customer_Offer_Detail__c cood : coodList){
            coodIdVOODListMap.put(cood.Id, cood.VO_Offer_Details__r);
        }
        
        List<Voucher__c> voucherList = new List<Voucher__c>();
        for (Customer_Offer_Transaction__c ot : newList){
            // Distribute Redemption Credits
            Integer redCredits = Integer.valueOf(ot.Redemption_Credits__c);
            for (VO_Offer_Detail__c vood : coodIdVOODListMap.get(ot.CustomerOffer__c)){
                if (vood.Customer_Credits_Available__c >= redCredits) {
                    // if the first one is enough credits use this for the voucher
                    Voucher__c voucher = new Voucher__c(RecordTypeId = CUSTOMERVOUCHERRECORDTYPEID);
                    voucher.Customer_Credits_Consumed__c = redCredits;
                    voucher.CustomerOfferDetail__c = ot.CustomerOffer__c;
                    voucher.OfferTransaction__c = ot.Id;
                    voucher.VehicleOwnership__c = vood.Vehicle_Ownership__c;
                    voucher.VO_Offer_Details__c = vood.Id;
                    voucher.Vin_Offer_Detail__c = vood.Vin_Offer_Detail__c;
                    voucher.Voucher_Type__c = ot.Redemption_Type__c;
                    voucherList.add(voucher);
                    break;
                }
                else { // if it is not enough, just fill it up
                    Integer voucherNum = Integer.valueOf(vood.Customer_Credits_Available__c); // use this for the voucher
                    Voucher__c voucher = new Voucher__c(RecordTypeId = CUSTOMERVOUCHERRECORDTYPEID);
                    voucher.CustomerOfferDetail__c = ot.CustomerOffer__c;
                    voucher.Customer_Credits_Consumed__c = voucherNum;
                    voucher.OfferTransaction__c = ot.Id;
                    voucher.Voucher_Type__c = ot.Redemption_Type__c;
                    voucher.VehicleOwnership__c = vood.Vehicle_Ownership__c;
                    voucher.VO_Offer_Details__c = vood.Id;
                    voucher.Vin_Offer_Detail__c = vood.Vin_Offer_Detail__c;
                    voucherList.add(voucher);
                    redCredits = redCredits - voucherNum;
                }
            }
        }
        
        // voucher insert
        if (voucherList.size() >0){
            insert voucherList;
        }   
    }
    /* MAY NO LONGER BE NEEDED
    public static void preventCOTDeletion(List<Customer_Offer_Transaction__c> oldList){
        for (Customer_Offer_Transaction__c cot : oldList){
            if (!cot.Delete_Record__c){
                cot.addError(Label.COTDeletionError);
            }
        }
    }*/
}